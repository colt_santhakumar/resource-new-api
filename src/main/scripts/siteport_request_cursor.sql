Set serveroutput on

DECLARE
resourceId NUMBER(10, 0);
novitasServiceId VARCHAR2(255 CHAR);
resourceName VARCHAR2(255 CHAR);
resourceStatus VARCHAR2(255 CHAR);

CURSOR siteport_resources_cursor IS
SELECT rs.ID, spr.SERVICE_ID, sprp.PORT_NAME, spr.STATUS
FROM DEV_NOVITAS_RESOURCES.REQUEST_SEQUENCE rs
  INNER JOIN DEV_NOVITAS_REQUESTS.SITEPORT_REQUEST spr ON spr.REQUEST_ID = rs.ID
  INNER JOIN DEV_NOVITAS_REQUESTS.SITEPORT_REQUEST_PORT sprp ON sprp.SITE_PORT_REQUEST_ID = spr.REQUEST_ID
WHERE rs.TYPE = 'SITE_PORT' and (rs.NOVITAS_SERVICE_ID IS NULL or rs.STATUS IS NULL or rs.NAME IS NULL);

begin
OPEN siteport_resources_cursor;
LOOP
FETCH siteport_resources_cursor into resourceId, novitasServiceId, resourceName, resourceStatus;
EXIT WHEN siteport_resources_cursor%notfound;
dbms_output.put_line(resourceId || ' ' || novitasServiceId || ' ' || resourceName || ' ' || resourceStatus);

UPDATE DEV_NOVITAS_RESOURCES.REQUEST_SEQUENCE rs SET rs.NOVITAS_SERVICE_ID = novitasServiceId, rs.NAME = resourceName, rs.STATUS = resourceStatus
WHERE rs.ID = resourceId;

END LOOP;
COMMIT;
CLOSE siteport_resources_cursor;
end;