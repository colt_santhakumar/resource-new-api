Set serveroutput on

DECLARE
resourceId NUMBER(10, 0);
novitasServiceId VARCHAR2(255 CHAR);
resourceName VARCHAR2(255 CHAR);
resourceStatus VARCHAR2(255 CHAR);

CURSOR oracle_resources_cursor IS
SELECT ss.ID, ps.NOVITAS_SERVICE_ID, ps.PORT_NAME, ps.STATUS
FROM DEV_NOVITAS_RESOURCES.SERVICE_SEQUENCE ss
  INNER JOIN DEV_NOVITAS_ORACLE.PORT_SERVICE ps ON ps.PORT_ID = ss.ID
WHERE ss.TYPE = 'ORACLE_PORT' and (ss.NOVITAS_SERVICE_ID IS NULL or ss.STATUS IS NULL or ss.NAME IS NULL);

begin
OPEN oracle_resources_cursor;
LOOP
FETCH oracle_resources_cursor into resourceId, novitasServiceId, resourceName, resourceStatus;
EXIT WHEN oracle_resources_cursor%notfound;
dbms_output.put_line(resourceId || ' ' || novitasServiceId || ' ' || resourceName || ' ' || resourceStatus);

UPDATE DEV_NOVITAS_RESOURCES.SERVICE_SEQUENCE ss SET ss.NOVITAS_SERVICE_ID = novitasServiceId, ss.NAME = resourceName, ss.STATUS = resourceStatus
WHERE ss.ID = resourceId;

END LOOP;
COMMIT;
CLOSE oracle_resources_cursor;
end;

