Set serveroutput on

DECLARE
resourceId NUMBER(10, 0);
novitasServiceId VARCHAR2(255 CHAR);
resourceName VARCHAR2(255 CHAR);
resourceStatus VARCHAR2(255 CHAR);

CURSOR ipaccess_resources_cursor IS
SELECT rs.ID, pr.NOVITAS_SERVICE_ID, pr.CONNECTION_NAME, pr.STATUS
FROM DEV_NOVITAS_RESOURCES.REQUEST_SEQUENCE rs
  INNER JOIN DEV_NOVITAS_IPACCESS.IP_ACCESS_REQUEST pr ON pr.REQUEST_ID = rs.ID
WHERE rs.TYPE = 'IPACCESS_WIRESONLY' and (rs.NOVITAS_SERVICE_ID IS NULL or rs.STATUS IS NULL or rs.NAME IS NULL);

begin
OPEN ipaccess_resources_cursor;
LOOP
FETCH ipaccess_resources_cursor into resourceId, novitasServiceId, resourceName, resourceStatus;
EXIT WHEN ipaccess_resources_cursor%notfound;
dbms_output.put_line(resourceId || ' ' || novitasServiceId || ' ' || resourceName || ' ' || resourceStatus);

UPDATE DEV_NOVITAS_RESOURCES.REQUEST_SEQUENCE rs SET rs.NOVITAS_SERVICE_ID = novitasServiceId, rs.NAME = resourceName, rs.STATUS = resourceStatus
WHERE rs.ID = resourceId;

END LOOP;
COMMIT;
CLOSE ipaccess_resources_cursor;
end;