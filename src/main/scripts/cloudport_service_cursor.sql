Set serveroutput on

DECLARE
resourceId NUMBER(10, 0);
novitasServiceId VARCHAR2(255 CHAR);
resourceName VARCHAR2(255 CHAR);
resourceStatus VARCHAR2(255 CHAR);

CURSOR cloudport_resources_cursor IS
SELECT ss.ID, ps.SERVICE_ID, ps.NAME, ps.STATUS
FROM DEV_NOVITAS_RESOURCES.SERVICE_SEQUENCE ss
  INNER JOIN DEV_NOVITAS_SERVICES.CUSTOMER_CLOUD_PORT ps ON ps.CUST_PORT_ID = ss.ID
WHERE ss.TYPE = 'CLOUD_PORT' and (ss.NOVITAS_SERVICE_ID IS NULL or ss.STATUS IS NULL or ss.NAME IS NULL);

begin
OPEN cloudport_resources_cursor;
LOOP
FETCH cloudport_resources_cursor into resourceId, novitasServiceId, resourceName, resourceStatus;
EXIT WHEN cloudport_resources_cursor%notfound;
dbms_output.put_line(resourceId || ' ' || novitasServiceId || ' ' || resourceName || ' ' || resourceStatus);

UPDATE DEV_NOVITAS_RESOURCES.SERVICE_SEQUENCE ss SET ss.NOVITAS_SERVICE_ID = novitasServiceId, ss.NAME = resourceName, ss.STATUS = resourceStatus
WHERE ss.ID = resourceId;

END LOOP;
COMMIT;
CLOSE cloudport_resources_cursor;
end;