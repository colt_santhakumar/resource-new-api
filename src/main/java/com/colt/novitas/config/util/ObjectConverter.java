package com.colt.novitas.config.util;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.dozer.Mapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ObjectConverter {

    @Autowired
    private Mapper mapper;

    public <F, T> List<T> convert(List<F> fromList, final Class<T> toClass) {
        return Lists.transform(fromList, new Function<F, T>() {
            public T apply(F from) {
                return convert(from, toClass);
            }
        });
    }


    public <F, T> T convert(F from, final Class<T> toClass) {
        if (from == null) return null;
        try{
            return mapper.map(from, toClass);
        }catch(MappingException ex){
            throw new MappingException(ex.getCause());
        }
    }

}
