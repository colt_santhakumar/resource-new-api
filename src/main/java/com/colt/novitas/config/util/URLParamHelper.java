package com.colt.novitas.config.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class URLParamHelper {
	
	public static String decodeParams(String param) {

		String decodedParam = null;
		try {
			decodedParam = URLDecoder.decode(param, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decodedParam;
	}

}
