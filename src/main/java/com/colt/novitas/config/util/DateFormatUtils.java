package com.colt.novitas.config.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

public class DateFormatUtils {

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

    private static final String LOCAL_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssz";

    public static Date formatLDTString(String strDate) {
        if (strDate == null) {
            return null;
        }

        java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern(LOCAL_DATE_TIME_FORMAT);
        LocalDateTime ldt = LocalDateTime.parse(strDate, formatter);
        return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date formatStringToDateWithTime(String strDate) {

        DateFormat df = new SimpleDateFormat(DATE_FORMAT);

        if (strDate != null) {
            try {
                return df.parse(strDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getCurrentDateInUtc() {
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(new Date());
    }

    public static String formatDate(Date date) {
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (date != null) {
            return df.format(date);
        } else {
            return "";
        }
    }
}
