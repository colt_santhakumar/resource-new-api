package com.colt.novitas.config.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.listener.FatalExceptionStrategy;
import org.springframework.amqp.rabbit.listener.exception.ListenerExecutionFailedException;
import org.springframework.messaging.converter.MessageConversionException;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;

@Slf4j
public class InvalidExchangeMessage implements FatalExceptionStrategy {

    @Override
    public boolean isFatal(Throwable throwable) {
    	   	
        if (throwable instanceof ListenerExecutionFailedException) {
            ListenerExecutionFailedException ex = (ListenerExecutionFailedException) throwable;

            if (ex.getCause() instanceof MessageConversionException
                    || ex.getCause() instanceof MethodArgumentNotValidException) {
                
				log.warn("Fatal message occurred while processing rabbitMQ message: {}. Message dropped!!!", new String(ex.getFailedMessage().getBody()));

                return true;
            }

            return ex.getFailedMessage().getMessageProperties().isRedelivered();
        }

        return false;
    }
}
