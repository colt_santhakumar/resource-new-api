/**
 * 
 */
package com.colt.novitas.config.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author omerio
 *
 */
@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class UnauthorisedException extends Exception {

    private static final long serialVersionUID = -7899571689125218591L;

    public UnauthorisedException() {
        super();
    }

    public UnauthorisedException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public UnauthorisedException(String arg0) {
        super(arg0);
    }

    public UnauthorisedException(Throwable arg0) {
        super(arg0);
    }

}
