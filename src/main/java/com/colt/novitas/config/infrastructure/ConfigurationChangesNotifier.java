package com.colt.novitas.config.infrastructure;

import java.util.Map;

public interface ConfigurationChangesNotifier {
     void sendConfigurationChangesNotification(Map<String, String> properties);
}
