package com.colt.novitas.config.infrastructure.handlers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.Objects;

@Slf4j
public class MessageQueueArrivalCallbackHandler implements RabbitTemplate.ReturnCallback {

    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        String messageBody =  Objects.nonNull(message.getBody()) ? new String(message.getBody()) : null;
        String messageId = Objects.nonNull(message.getMessageProperties()) ? message.getMessageProperties().getMessageId() : null;

        log.info("Message has not been sent to the queue: {}. MessageId: {}. Payload: {}. ReplyCode: {}. ReplyText: {}",
                exchange + "." + routingKey, messageId, messageBody, replyText
        );
    }
}
