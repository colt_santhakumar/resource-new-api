package com.colt.novitas.config.infrastructure;

import com.colt.novitas.config.service.IdService;
import com.colt.novitas.config.infrastructure.pojos.ResourceChangeMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Slf4j
public class ResourceChangesMessageReceiver {

    private static final String RESOURCE_CHANGE = "resource_change";
    private static final String SERVICE_RESOURCE = "service";
    private static final String REQUEST_RESOURCE = "request";

    @Autowired
    private IdService idService;

    @RabbitListener(queues = "${resource_changes_queue_name}", containerFactory = "rabbitListenerContainerFactory")
    public void processResourceChange(ResourceChangeMessage resourceChanges) {

        log.info("Received message: {}",resourceChanges);

        validateMessage(resourceChanges);

        if (RESOURCE_CHANGE.equals(resourceChanges.getMessageType())) {

            if (SERVICE_RESOURCE.equals(resourceChanges.getResourceType())) {
                idService.updateServiceId(resourceChanges);
            }

            if (REQUEST_RESOURCE.equals(resourceChanges.getResourceType())) {
                idService.updateRequestId(resourceChanges);
            }
        }
    }

    private void validateMessage(ResourceChangeMessage resourceChangeMessage) {
        if (!RESOURCE_CHANGE.equals(resourceChangeMessage.getMessageType())) {
            throw new AmqpRejectAndDontRequeueException("resource_type is invalid!");
        }

        if (Objects.isNull(resourceChangeMessage.getTimestamp())) {
            throw new AmqpRejectAndDontRequeueException("timestamp is invalid!");
        }

        if (StringUtils.isEmpty(resourceChangeMessage.getNewName())
                && StringUtils.isEmpty(resourceChangeMessage.getStatus())) {
            throw new AmqpRejectAndDontRequeueException("Either status or newName should be set!");
        }
    }
}
