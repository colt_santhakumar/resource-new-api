package com.colt.novitas.config.infrastructure.handlers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.Objects;

@Slf4j
public class MessageBrokerArrivalCallbackHandler implements RabbitTemplate.ConfirmCallback {

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {

        String messageId = Objects.nonNull(correlationData) ? correlationData.getId() : null;

        if (!ack) {

            log.info("Message has not been acknowledged by broker exchange. MessageId: {}. Cause: {}", messageId, cause);

        } else {

            log.info("Message has been acknowledged by broker exchange. MessageId: {}", messageId);

        }
    }
}
