package com.colt.novitas.config.infrastructure;

import com.colt.novitas.config.infrastructure.pojos.RequestChangeEventNotification;
import com.colt.novitas.config.service.IdService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Slf4j
public class RequestChangesMessageReceiver {

    @Autowired
    private IdService idService;

    @RabbitListener(queues = "${request_changes_queue_name}", containerFactory = "rabbitListenerContainerFactory")
    public void processRequestChange(RequestChangeEventNotification requestChangeEventNotification) {

        log.info("Received message: {}", requestChangeEventNotification);

        validateMessage(requestChangeEventNotification);

        idService.updateRequestWithRequestChangeEvent(requestChangeEventNotification);
    }
    
    private void validateMessage(RequestChangeEventNotification requestChangeEventNotification) {
        if (Objects.isNull(requestChangeEventNotification.getResourceId())) {
            throw new AmqpRejectAndDontRequeueException("resource_id is invalid!");
        }

        if (Objects.isNull(requestChangeEventNotification.getTimestamp())) {
            throw new AmqpRejectAndDontRequeueException("timestamp is invalid!");
        }
    
        if (Objects.isNull(requestChangeEventNotification.getResourceUrl())) {
            throw new AmqpRejectAndDontRequeueException("resource_url is invalid!");
        }
    
        if (Objects.isNull(requestChangeEventNotification.getProductType())) {
            throw new AmqpRejectAndDontRequeueException("product_type is invalid!");
        }
    }
}
