package com.colt.novitas.config.infrastructure.pojos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets ResourceType
 */
public enum ResourceType {
  GOOGLE_PORT("GOOGLE_PORT"),
    IBM_PORT("IBM_PORT"),
    AWS_HOSTED_PORT("AWS_HOSTED_PORT"),
    AWS_DEDICATED_PORT("AWS_DEDICATED_PORT"),
    AZURE_PORT("AZURE_PORT"),
    ETHERNET_PORT("ETHERNET_PORT"),
    ORACLE_PORT("ORACLE_PORT"),
    EQUINIX_PORT("EQUINIX_PORT");

  private String value;

  ResourceType(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ResourceType fromValue(String text) {
    for (ResourceType b : ResourceType.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}
