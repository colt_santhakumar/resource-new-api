package com.colt.novitas.config.infrastructure.impl;

import com.colt.novitas.config.infrastructure.ConfigurationChangesNotifier;
import com.colt.novitas.config.infrastructure.pojos.ConfigurationChangeNotification;
import com.colt.novitas.config.util.DateFormatUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class ConfigurationChangesNotifierImpl implements ConfigurationChangesNotifier {

    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    private static final String BROKER_EXCHANGE_CONFIGURATIONS = "broker.exchange.configurations";
    private static final String BROKER_EXCHANGE_Configurations_BINDINGS = "";

    private static final String MESSAGE_TYPE = "configuration_changed";

    @Override
    @Async
    public void sendConfigurationChangesNotification(Map<String, String> properties) {

        EnvValue envValue = getBrokerExchangeEnvValue(properties);
        String brokerExchangeConfigurations = envValue.value;

        ConfigurationChangeNotification configChangeNotification = new ConfigurationChangeNotification();
        configChangeNotification.setEnvironment(envValue.env);
        configChangeNotification.setMessageType(MESSAGE_TYPE);

        String currentDate = DateFormatUtils.getCurrentDateInUtc();
        String messageId = MESSAGE_TYPE + ":" + currentDate;

        String jsonMessage = getConfigurationChangeNotificationAsJson(configChangeNotification);

        Message configChangeNotificationMessage = MessageBuilder.withBody(jsonMessage != null ? jsonMessage.getBytes() : configChangeNotification.toString().getBytes())
                .setContentType(MessageProperties.CONTENT_TYPE_JSON)
                .setMessageId(messageId)
                .build();

        try {
            rabbitTemplate.convertAndSend(brokerExchangeConfigurations, BROKER_EXCHANGE_Configurations_BINDINGS, configChangeNotificationMessage, (message) -> {
                    message.getMessageProperties().setCorrelationId(messageId);
                    message.getMessageProperties().setContentType(MessageProperties.CONTENT_TYPE_JSON);
                    Date currentDateUtc = DateFormatUtils.formatStringToDateWithTime(currentDate);
                    message.getMessageProperties().setTimestamp(currentDateUtc);
                    return message;
                }, new CorrelationData(messageId));

            log.info("Configuration message change: {}, has been sent to: {}", jsonMessage, brokerExchangeConfigurations);

        } catch (Exception e) {
            log.error("Sending payload: {}, with messageId: {}, to queue: {} has failed", jsonMessage, messageId, brokerExchangeConfigurations + "." + BROKER_EXCHANGE_Configurations_BINDINGS, e);
        }
    }

    private EnvValue getBrokerExchangeEnvValue(Map<String, String> properties) {
        return properties.keySet()
                .stream()
                .filter(key -> key.contains(BROKER_EXCHANGE_CONFIGURATIONS))
                .map(key -> {

                    EnvValue envValue = new EnvValue();

                    String[] envKey = key.split("::");

                    if (envKey.length > 0) {
                        envValue.env = envKey[0];
                    }

                    envValue.value = properties.get(key);

                    return envValue;
                })
                .findFirst()
                .orElse(null);
    }

    private String getConfigurationChangeNotificationAsJson(ConfigurationChangeNotification configurationChangeNotification) {
        try {
            return objectMapper.writeValueAsString(configurationChangeNotification);
        } catch (JsonProcessingException e) {
            log.error("ConfigurationChangeNotification: {} failed to be serialized as json.", configurationChangeNotification, e);
            return null;
        }
    }

    private static class EnvValue {
        private String env;
        private String value;
    }
}
