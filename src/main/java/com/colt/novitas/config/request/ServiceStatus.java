package com.colt.novitas.config.request;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ServiceStatus {
    ACTIVE("ACTIVE");

    private static final Map<String, ServiceStatus> FORMAT_MAP = Stream
            .of(ServiceStatus.values())
            .collect(Collectors.toMap(s -> s.status, Function.identity()));

    private final String status;

    ServiceStatus(String status) {
        this.status = status;
    }

    @JsonCreator
    public static ServiceStatus fromString(String string) {
        return Optional.ofNullable(FORMAT_MAP.get(string)).orElseThrow(() -> new IllegalArgumentException(string));
    }
}
