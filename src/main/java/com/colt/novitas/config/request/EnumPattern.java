package com.colt.novitas.config.request;

import javax.validation.*;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@ReportAsSingleViolation
@Documented
@Constraint(validatedBy = EnumPattern.EnumPatternValidator.class)
public @interface EnumPattern {
    String regexp();

    String message() default "request field not match with {regexp}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    boolean nullable() default true;

    class EnumPatternValidator implements ConstraintValidator<EnumPattern, Enum<?>> {
        private Pattern pattern;
        boolean nullable;

        @Override
        public void initialize(EnumPattern annotation) {
            nullable = annotation.nullable();
            try {
                pattern = Pattern.compile(annotation.regexp());
            } catch (PatternSyntaxException e) {
                throw new IllegalArgumentException("Given regex is invalid", e);
            }
        }

        @Override
        public boolean isValid(Enum<?> anEnum, ConstraintValidatorContext context) {
            if (anEnum == null) {
                return nullable;
            }

            Matcher m = pattern.matcher(anEnum.name());
            return m.matches();
        }
    }
}
