package com.colt.novitas.config.request;

import com.colt.novitas.config.restclients.model.RequestAction;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
@ToString
public class Request {

    private Integer id;

    private String name;

    private String type;

    private String novitasServiceId;

    private String status;
    
    private String lastUpdated;
    
    private String description;
    
    private String detailUrl;
    
    private String requestedBy;
    
    private RequestAction action;
    
    private String requestedAt;
    
    private Integer commitmentPeriod;
    
    private String currency;
    
    private Float decommissioningCharge;
    
    private Float installationCharge;
    
    private Float penaltyCharge;
    
    private Float rentalCharge;
    
    private String rentalUnit;

    private String serviceId;

    private Integer bandwidth;

    private String siteName;

    private String cspKey;
}
