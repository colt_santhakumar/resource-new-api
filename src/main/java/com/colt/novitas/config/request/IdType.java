package com.colt.novitas.config.request;

public enum IdType {
   
	PORT,
	CONNECTION,
	SITE_PORT,
	CROSS_CONNECT,
	CLOUD_PORT, 
	CLOUD_CONNECTION,
	NOVITAS_SERVICE, 
	TEMP_SITE,
	GOOGLE_PORT,
	IBM_PORT,
	IPACCESS_WIRESONLY,
	VAS,
	ORACLE_PORT,
	EQUINIX_PORT;
}
