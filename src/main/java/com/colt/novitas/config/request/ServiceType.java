package com.colt.novitas.config.request;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ServiceType {
    PORT("PORT");

    private static Map<String, ServiceType> FORMAT_MAP = Stream
            .of(ServiceType.values())
            .collect(Collectors.toMap(s -> s.type, Function.identity()));

    private final String type;

    ServiceType(String type) {
        this.type = type;
    }

    @JsonCreator
    public static ServiceType fromString(String string) {
        return Optional.ofNullable(FORMAT_MAP.get(string)).orElseThrow(() -> new IllegalArgumentException(string));
    }
}
