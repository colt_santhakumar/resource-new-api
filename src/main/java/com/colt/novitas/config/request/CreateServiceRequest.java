package com.colt.novitas.config.request;


import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class CreateServiceRequest {

    @NotBlank(message = "id is mandatory field")
    @Pattern(regexp = "^[0-9]*$", message = "id must be number string ")
    private String id;

    @NotBlank(message = "name is mandatory field")
    private String name;


    @EnumPattern(regexp = "PORT", nullable = false)
    private ServiceType type;

    @NotBlank(message = "parent_service_id is mandatory field")
    @Pattern(regexp = "^[0-9]*$", message = "parent_service_id must be number string")
    private String parent_service_id;  //NOSONAR

    @EnumPattern(regexp = "ACTIVE", nullable = false)
    private ServiceStatus status;

}
