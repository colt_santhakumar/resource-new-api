package com.colt.novitas.config.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
@ToString
public class RequestErrorDetailsRequest {
	private Integer requestId;
	private String status;
	private String deploymentId;
	private byte[] errorStack;
	private Long processInstanceId;
	private String hostAddress;
    private String taskName;
}
