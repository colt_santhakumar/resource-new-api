package com.colt.novitas.config.restclients.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
@ToString
public class RequestCommonAttributes {
    
    private String novitasServiceId;
    
    private String serviceId;
    
    private String name;
    
    private String status;
    
    private String action;
    
    private String requestedAt;
    
    private String description;
    
    private String lastUpdated;
    
    private String requestedBy;
    
    private Integer bandwidth;
    
    private Float rentalCharge;
    
    private String currency;
    
    private String rentalUnit;
    
    private Float installationCharge;
    
    private Float decommissioningCharge;
    
    private Integer commitmentPeriod;
    
    private Float penaltyCharge;

    private String siteName;

    private String cspKey;

}
