package com.colt.novitas.config.restclients.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets RequestAction
 */
public enum RequestAction {
  CREATE("CREATE"),
    UPDATE("UPDATE"),
    DELETE("DELETE");

  private String value;

  RequestAction(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static RequestAction fromValue(String text) {
    for (RequestAction b : RequestAction.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}
