package com.colt.novitas.config.restclients;

import com.colt.novitas.config.infrastructure.pojos.RequestChangeEventNotification;
import com.colt.novitas.config.restclients.model.RequestCommonAttributes;
import com.colt.novitas.config.service.ConfigurationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class ApiClient {
    
    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private ConfigurationService configurationService;
    
    @Autowired
    private Environment environment;
    
    private static final String CONFIG_PROFILE = "spring.profiles.active";
    private static final String AUTH_USERNAME_SUFFIX = ".auth.username";
    private static final String AUTH_PASSWORD_SUFFIX = ".auth.password";
    
    public RequestCommonAttributes getRequestByUrl(RequestChangeEventNotification eventNotification) {

        StringBuilder usernameKey = new StringBuilder();
        StringBuilder passwordKey = new StringBuilder();
        
        if(null != eventNotification.getProductType()) {
            String[] split = eventNotification.getProductType().name().split("_", 2);
            if(split.length >= 1) {
                usernameKey.append(split[0].toLowerCase()).append(AUTH_USERNAME_SUFFIX);
                passwordKey.append(split[0].toLowerCase()).append(AUTH_PASSWORD_SUFFIX);
            }
            log.info("Username auth key: {}, Password auth key: {}", usernameKey, passwordKey);
        }
        
        String env = environment.getProperty(CONFIG_PROFILE);
        
        log.info("Calling {} URI: {} to get request details", eventNotification.getProductType(), eventNotification.getResourceUrl());
        
        try {
            ResponseEntity<RequestCommonAttributes> request = restTemplate.exchange(
                    eventNotification.getResourceUrl(),
                    HttpMethod.GET,
                    new HttpEntity(AuthHeaderGenerator.createBasicAuthHeaders(
                            configurationService.getValue(env, usernameKey.toString()),
                            configurationService.getValue(env, passwordKey.toString()))),
                    RequestCommonAttributes.class);
            return request.getBody();
            
        } catch (Exception e) {
            log.error("Exception occurred while calling :::> {} :::> {}", eventNotification.getResourceUrl(), e);
            return null;
        }
    }
}
