package com.colt.novitas.config.restclients;

import org.slf4j.MDC;
import org.springframework.http.HttpHeaders;

import java.nio.charset.Charset;
import java.util.Base64;

public class AuthHeaderGenerator {

    public static HttpHeaders createBasicAuthHeaders(String username, String password) {
        return new HttpHeaders() {
            {
                final String auth = username + ":" + password;
                final byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
                final String authHeader = "Basic " + new String(encodedAuth);
                set("Authorization", authHeader);
                if (MDC.get("novitasApiId") != null) {
                    set("x-NovitasApiId", MDC.get("novitasApiId"));
                }
            }
        };
    }

    public static HttpHeaders createBearerTokenAuthHeaders(String token) {
        return new HttpHeaders() {
            {
                final String authHeader = "Bearer " + token;
                set("Authorization", authHeader);
            }
        };
    }
}
