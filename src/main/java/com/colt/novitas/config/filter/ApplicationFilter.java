package com.colt.novitas.config.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@Slf4j
public class ApplicationFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		
		log.trace("x-NovitasApiId header value -->"+request.getHeader("x-NovitasApiId"));
	
		String portalApiId =request.getHeader("x-NovitasApiId");
		if (portalApiId == null){
			portalApiId="0";
		}

		MDC.put("novitasApiId", portalApiId);

		filterChain.doFilter(request, response);
	}
}