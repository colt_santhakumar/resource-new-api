package com.colt.novitas.config.controller;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class StatusCheckController {

	private static final Logger LOGGER = Logger.getLogger(StatusCheckController.class.getSimpleName());

	@RequestMapping(value = "/statuscheck",method = RequestMethod.GET)
	public ResponseEntity<?> getStatusCheck() throws Exception {
		LOGGER.info("Calling status check");
			return new ResponseEntity<>(HttpStatus.OK);
		
	}
	
	
}
