package com.colt.novitas.config.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.colt.novitas.config.request.RequestErrorDetailsRequest;
import com.colt.novitas.config.service.RequestErrorDetailsService;

@Controller
@RequestMapping("/request/error")
public class RequestErrorDetailsController {

	private static final Logger LOGGER = Logger.getLogger(RequestErrorDetailsController.class);

	@Autowired
	private RequestErrorDetailsService srv;

	@RequestMapping(value = "/{request_id:[0-9]*}", method = RequestMethod.GET)
	public ResponseEntity<RequestErrorDetailsRequest> getRequestErrorStack(
			@PathVariable(value = "request_id") Integer requestId) throws Exception {
		LOGGER.debug("Getting Error stack details" + requestId);
		return new ResponseEntity<>(srv.getRequestErrors(requestId), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> createErrorTriage(@RequestBody RequestErrorDetailsRequest req) throws Exception {
		LOGGER.debug("Creating Error entry for req" + req);
		srv.save(req);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> updateErrorTriage(@RequestBody RequestErrorDetailsRequest req) throws Exception {
		LOGGER.debug("update Error entry for req" + req);
		srv.update(req);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
