package com.colt.novitas.config.controller;

import com.colt.novitas.config.request.CreateServiceRequest;
import com.colt.novitas.config.service.ServiceSequence;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/services")
@Slf4j
public class ServiceController {
    @Autowired
    private ServiceSequence serviceSequence;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getServices(@RequestParam("name") String name,
                                         @RequestParam("novitas_service_id") String novitasServiceId) {

        log.debug("Getting services matching name: {} and novitasServiceId : {}", name, novitasServiceId );
        return new ResponseEntity<> (serviceSequence.getServicesByNameAndNovitasServiceId(name, novitasServiceId), HttpStatus.OK);
    }

    @RequestMapping(value="/all", method = RequestMethod.GET)
    public ResponseEntity<?> getServicesByIds(@RequestParam("service_id") List<String> serviceIds) {

        log.debug("Getting services matching ids");
        return new ResponseEntity<> (serviceSequence.getServicesByServiceIds(serviceIds), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> createService(@Valid @RequestBody CreateServiceRequest request) {
        serviceSequence.createServiceManual(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}
