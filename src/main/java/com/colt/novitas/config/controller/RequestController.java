package com.colt.novitas.config.controller;

import com.colt.novitas.config.service.RequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/requests")
@Slf4j
public class RequestController {

    @Autowired
    private RequestService requestService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getRequests(@RequestParam(value = "name", required = false) String name,
                                         @RequestParam(value = "novitas_service_id") String novitasServiceId,
                                         @RequestParam(value = "status", required = false) List<String> status,
                                         @RequestParam(value = "type", required = false) String type) {

        log.info("Getting requests matching name: {}  novitasServiceId : {}  status: {}  type: {}", name, novitasServiceId, status, type );
        return new ResponseEntity<> (requestService.getRequestsBySearchCriteria(name, novitasServiceId, status, type), HttpStatus.OK);
    }
}
