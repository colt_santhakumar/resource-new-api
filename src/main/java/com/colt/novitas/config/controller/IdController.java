package com.colt.novitas.config.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.colt.novitas.config.request.IdType;
import com.colt.novitas.config.service.IdService;

@Controller
@RequestMapping("/id")
public class IdController {

	private static final Logger LOGGER = Logger.getLogger(IdController.class);

	@Autowired
	private IdService srv;

	
	@RequestMapping(value = "/request",method = RequestMethod.POST)
	public ResponseEntity<Integer> createRequestId(@RequestParam(name="id_type", required = true) IdType type) throws Exception {
		LOGGER.debug("Creating Request Id for type" + type);
		return srv.createRequestId(type).map(id -> new ResponseEntity<Integer>(id, HttpStatus.CREATED))
				.orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
		            
	}
	
	@RequestMapping(value = "/service",method = RequestMethod.POST)
	public ResponseEntity<String> createServiceId(@RequestParam(name="id_type", required = true) IdType type) throws Exception {
		LOGGER.debug("Creating Service Id for type" + type);
		return srv.createServiceId(type).map(id -> new ResponseEntity<String>(id, HttpStatus.CREATED))
				.orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
	}
	
	@RequestMapping(value = "/request/{request_id:[0-9]*}",method = RequestMethod.GET)
	public ResponseEntity<String> getRequestIdType(@PathVariable(value = "request_id") Integer id) throws Exception {
		LOGGER.debug("Get Request Id type" + id);
		return srv.getRequestIdType(id).map(type -> new ResponseEntity<>(type.name(), HttpStatus.OK))
				.orElseGet( () -> new ResponseEntity<>(HttpStatus.NOT_FOUND) ); 
	}
	
	@RequestMapping(value = "/service/{service_id:[0-9]*}",method = RequestMethod.GET)
	public ResponseEntity<String> getServiceIdType(@PathVariable(value = "service_id") String id) throws Exception {
		LOGGER.debug("Get Service Id type" + id);
		return srv.getServiceIdType(id).map(type -> new ResponseEntity<>(type.name(), HttpStatus.OK))
				.orElseGet( () -> new ResponseEntity<>(HttpStatus.NOT_FOUND) ); 
	}
	
    @RequestMapping(method = RequestMethod.GET, value = "/request/id_range" )
	public ResponseEntity<?> getRequestById(@RequestParam(value = "begin_at", required = false) Integer beginAt,
			@RequestParam(value = "end_at", required = false) Integer endAt) {

		LOGGER.debug("Calling getRequestIds()");
		try{
		 Map<Integer,IdType> resp =	srv.getRequestIds(null == beginAt || beginAt <=1 ? 1 : beginAt, null == endAt || endAt <=100 ? 100 : endAt);
		 return new ResponseEntity<Map<Integer,IdType>>(resp,HttpStatus.OK);
		}catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
}
