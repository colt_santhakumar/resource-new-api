/**
 * 
 */
package com.colt.novitas.config.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.colt.novitas.config.service.ConfigurationService;

/**
 * 
 * GET /api/configuration?environment=DEV&item=request.api.username
 * 
 * @author omerio
 *
 */
@Controller
@RequestMapping(value = "/configuration")
public class ConfigurationController {

    private static final Logger LOG = Logger.getLogger(ConfigurationController.class);

    @Autowired
    private ConfigurationService service;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getItem(
            @RequestParam(value = "environment") String environment,
            @RequestParam(value = "item", required = false) List<String> items, HttpServletRequest request) throws Exception {

        String ipAddress = request.getRemoteAddr();

        LOG.info("Getting configuration item: " + items + ", for environment " + environment + ", request IP address: "
                + ipAddress);

        ResponseEntity<?> responseEntity;

        if (items != null && items.size() > 0) {
            if(items.size() == 1) {
                String item = items.get(0);
                String value = service.getValue(environment, item, ipAddress);
                responseEntity = new ResponseEntity<>(new Response(environment, item, value), HttpStatus.OK);

            } else {
                List<Response> responses = new ArrayList<>();
                for(String item: items) {
                    String value = service.getValue(environment, item, ipAddress);

                    responses.add(new Response(environment, item, value));
                }

                responseEntity = new ResponseEntity<>(responses, HttpStatus.OK);
            }
        } else {
            List<Response> responseItems = service.getItemsByEnvironment(environment, ipAddress);
            responseEntity = new ResponseEntity<>(responseItems, HttpStatus.OK);
        }

        return responseEntity;
    }

    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/reload", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity reload() throws Exception {

        LOG.info("Refreshing configurations... ");
        service.reload();

        return new ResponseEntity(HttpStatus.OK);
    }

}
