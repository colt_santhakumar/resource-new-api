/**
 * 
 */
package com.colt.novitas.config.controller;

/**
 * Response object to be serialized to the following json 
 * { 
 *      "environment": "DEV", 
 *      "item": "request.api.username", 
 *      "value": "acomplicatedusername" 
 * }
 * 
 * @author omerio
 *
 */
public class Response {

    private String environment;

    private String item;

    private String value;

    public Response() {
        super();
    }

    public Response(String environment, String item, String value) {
        super();
        this.environment = environment;
        this.item = item;
        this.value = value;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
