package com.colt.novitas.config.dao.criteria;

import com.colt.novitas.config.entity.ResourceEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Objects;
import java.util.function.BinaryOperator;

import static java.util.stream.Collectors.toList;

public class SpecificationBuilder<ENTITY extends ResourceEntity> {

    public Specification<ENTITY> getResourceQuerySpecification(List<SearchCriteria> searchCriteria) {
        return buildSearchCondition(searchCriteria, Specification::and);
    }

    private Specification<ENTITY> buildSearchCondition(List<SearchCriteria> searchCriteria, BinaryOperator<Specification<ENTITY>> operator) {
        if (CollectionUtils.isEmpty(searchCriteria)) {
            return null;
        }

        List<Specification<ENTITY>> specifications = searchCriteria.stream().map(this::buildSearchCondition).collect(toList());

        return specifications.stream().filter(Objects::nonNull).reduce(operator).orElse(null);
    }

    private Specification<ENTITY> buildSearchCondition(SearchCriteria searchCriteria) {
        return (root, query, criteriaBuilder) -> {

            Predicate predicate = null;

            final String key = searchCriteria.getKey();
            Path path;
            if (key.contains(SearchCriteria.INNER_OBJECT_DELIMITER)) {
                path = getCombinedPath(key, root);
            } else {
                path = root.get(key);
            }

            Object value = searchCriteria.getValue();

            switch (searchCriteria.getOperator()) {
                case EQUALITY:
                    if (Objects.isNull(searchCriteria.getValue())) {
                        predicate = criteriaBuilder.isNull(path);
                    } else {
                        predicate = criteriaBuilder.equal(path, value);
                    }
                    break;
                case NEGATION:
                    predicate = criteriaBuilder.notEqual(path, value);
                    break;
                case LIKE:
                    predicate = criteriaBuilder.like(path, "%" + value + "%");
                    break;
                case STARTS_WITH:
                    predicate = criteriaBuilder.like(path, value + "%");
                    break;
                case ENDS_WITH:
                    predicate = criteriaBuilder.like(path, "%" + value);
                    break;
                case LESS_THAN:
                    predicate = criteriaBuilder.lessThan(path, (Comparable) value);
                    break;
                case LESS_THAN_OR_EQUAL:
                    predicate = criteriaBuilder.lessThanOrEqualTo(path, (Comparable) value);
                    break;
                case GREATER_THAN:
                    predicate = criteriaBuilder.greaterThan(path, (Comparable) value);
                    break;
                case GREATER_THAN_OR_EQUAL:
                    predicate = criteriaBuilder.greaterThanOrEqualTo(path, (Comparable) value);
                    break;
                case IN:
                    assert path != null;
                    predicate = path.in((List) value);
                    break;
                case NOT_IN:
                    assert path != null;
                    predicate = path.in((Object[]) value).not();
                default:
            }
            return predicate;
        };
    }

    private Path<Object> getCombinedPath(String key, Root<ENTITY> root) {
        Path<Object> path = null;
        final String[] keys = key.split("\\" + SearchCriteria.INNER_OBJECT_DELIMITER);
        for (String value : keys) {
            if (path == null) {
                path = root.get(value);
            } else {
                path = path.get(value);
            }
        }
        return path;
    }
}
