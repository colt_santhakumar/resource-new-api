package com.colt.novitas.config.dao;

import com.colt.novitas.config.entity.RequestId;
import com.colt.novitas.config.entity.ServiceId;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class IdDaoImpl implements IdDao {
	
	@PersistenceContext
	private EntityManager entityManager;

	private Session getCurrentSession() {
		return entityManager.unwrap(Session.class);
	}

	@Override
	public Integer createRequestId(RequestId req) {
		return (Integer) getCurrentSession().save(req);
	}

	@Override
	public void updateRequestId(RequestId req) {
		Query query = getCurrentSession().createQuery(
				"UPDATE RequestId id SET id.lastUpdated = :lastUpdated, id.name = :name, id.status = :status WHERE id.id = :requestId"
						+ " AND (id.lastUpdated <= :lastUpdated OR id.lastUpdated is NULL)");
		query.setDate("lastUpdated", req.getLastUpdated());
		query.setString("name", req.getName());
		query.setString("status", req.getStatus());
		query.setInteger("requestId", req.getId());
		query.executeUpdate();
	}

	@Override
	public RequestId getRequestIdType(Integer requestId) {
		return getCurrentSession().get(RequestId.class,requestId);
	}

	@SuppressWarnings("unchecked")
	@Override
    public List<RequestId> getTopAllRequestsByRang(int begin, int end){

		final Query query = getCurrentSession().createQuery("FROM RequestId order by id  DESC");
		query.setFirstResult(begin-1);
		query.setMaxResults(end);
		return (List<RequestId>) query.list();

	}

	@Override
	public String createServiceId(ServiceId req) {
		return String.valueOf(getCurrentSession().save(req));
	}

	@Override
	public void updateServiceId(ServiceId service) {
		Query query = getCurrentSession().createQuery(
				"UPDATE ServiceId id SET id.lastUpdated = :lastUpdated, id.name = :name, id.status = :status WHERE id.id = :serviceId"
						+ " AND (id.lastUpdated <= :lastUpdated OR id.lastUpdated is NULL)");
		query.setDate("lastUpdated", service.getLastUpdated());
		query.setString("name", service.getName());
		query.setString("status", service.getStatus());
		query.setInteger("serviceId", service.getId());
		query.executeUpdate();
	}

	@Override
	public ServiceId getServiceIdType(String serviceId) {
		return getCurrentSession().get(ServiceId.class,Integer.parseInt(serviceId));
	}
}
