/**
 * 
 */
package com.colt.novitas.config.dao;

import java.util.Optional;

import com.colt.novitas.config.entity.RequestError;

/**
 * @author Dharma
 *
 */
public interface RequestErrorDetailsDao {

    Optional<RequestError> getRequestErrors(Integer requestId);

    void save(RequestError req);

    void update(RequestError req);
}
