package com.colt.novitas.config.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.colt.novitas.config.dao.RequestIdRepositoryCustom;
import com.colt.novitas.config.entity.RequestId;


@Repository
public class RequestIdRepositoryImpl implements RequestIdRepositoryCustom {
	
	@PersistenceContext
	private EntityManager entityManager;

	private Session getCurrentSession() {
		return entityManager.unwrap(Session.class);
	}

	@Override
	public Integer createRequestId(RequestId req) {
		return (Integer) getCurrentSession().save(req);
	}

	@Override
	public void updateRequestId(RequestId req) {
		Query query = getCurrentSession().createQuery(
				"UPDATE RequestId id SET id.lastUpdated = :lastUpdated, id.name = :name, id.status = :status WHERE id.id = :requestId"
						+ " AND (id.lastUpdated <= :lastUpdated OR id.lastUpdated is NULL)");
		query.setDate("lastUpdated", req.getLastUpdated());
		query.setString("name", req.getName());
		query.setString("status", req.getStatus());
		query.setInteger("requestId", req.getId());
		query.executeUpdate();
	}

	@Override
	public RequestId getRequestIdType(Integer requestId) {
		return  getCurrentSession().get(RequestId.class,requestId);
	}

	@SuppressWarnings("unchecked")
	@Override
    public List<RequestId> getTopAllRequestsByRang(int begin, int end){

		final Query query = getCurrentSession().createQuery("FROM RequestId order by id  DESC");
		if(begin <= 0) {
			query.setFirstResult(begin);
		}
		else {
			query.setFirstResult(begin-1);	
		}
		
		query.setMaxResults(end);
		return (List<RequestId>) query.list();

	}


}
