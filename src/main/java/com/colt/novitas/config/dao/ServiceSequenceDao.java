package com.colt.novitas.config.dao;

import com.colt.novitas.config.entity.ServiceId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceSequenceDao extends JpaRepository<ServiceId, String> {

    List<ServiceId> findByNameAndNovitasServiceId(String name, String novitasServiceId);

    @Query( "select s from ServiceId s where id in :ids" )
    List<ServiceId> findAllByServiceIds(@Param("ids") List<Integer> serviceIds);
}
