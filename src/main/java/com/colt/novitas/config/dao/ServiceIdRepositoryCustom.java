package com.colt.novitas.config.dao;

import com.colt.novitas.config.entity.ServiceId;

public interface ServiceIdRepositoryCustom {
	
	String createServiceId(ServiceId req);

	void updateServiceId(ServiceId req);

	ServiceId getServiceIdType(String serviceId);


}
