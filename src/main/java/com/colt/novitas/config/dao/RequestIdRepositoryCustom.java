package com.colt.novitas.config.dao;

import java.util.List;

import com.colt.novitas.config.entity.RequestId;

public interface RequestIdRepositoryCustom {
	
	Integer createRequestId(RequestId req);

	void updateRequestId(RequestId req);

	RequestId getRequestIdType(Integer requestId);

	List<RequestId> getTopAllRequestsByRang(int begin,int end);

}
