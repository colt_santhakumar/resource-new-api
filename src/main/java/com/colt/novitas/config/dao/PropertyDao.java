/**
 * 
 */
package com.colt.novitas.config.dao;

import java.util.List;

import com.colt.novitas.config.entity.Property;

/**
 * @author omerio
 *
 */
public interface PropertyDao {

    Property findByItemEnvironment(String item, String environment);

    List<Property> findAll();

    void save(Property property);
}
