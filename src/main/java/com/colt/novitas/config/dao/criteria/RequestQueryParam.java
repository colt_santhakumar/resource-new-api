package com.colt.novitas.config.dao.criteria;

import com.colt.novitas.config.entity.RequestId_;

public enum RequestQueryParam {

    STATUS(RequestId_.STATUS),
    NAME(RequestId_.NAME),
    TYPE(RequestId_.TYPE),
    NOVITAS_SERVICE_ID(RequestId_.NOVITAS_SERVICE_ID);

    private final String name;

    RequestQueryParam(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
