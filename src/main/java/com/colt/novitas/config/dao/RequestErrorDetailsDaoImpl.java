package com.colt.novitas.config.dao;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.colt.novitas.config.entity.RequestError;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class RequestErrorDetailsDaoImpl implements RequestErrorDetailsDao {

	@PersistenceContext
	private EntityManager entityManager;

	private Session getCurrentSession() {
		return entityManager.unwrap(Session.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Optional<RequestError> getRequestErrors(Integer requestId) {
		Query query = getCurrentSession().createQuery("From RequestError where requestId = :requestId and status = 'PENDING'");
		query.setInteger("requestId", requestId);

		List<RequestError> requestErrors = query.getResultList();

		return CollectionUtils.isEmpty(requestErrors) ? Optional.empty() : Optional.ofNullable(requestErrors.get(0));
	}

	@Override
	public void save(RequestError req) {
		req.setId(UUID.randomUUID().toString());
		getCurrentSession().save(req);
	}

	@Override
	public void update(RequestError req) {
		getCurrentSession().saveOrUpdate(req);
	}
}
