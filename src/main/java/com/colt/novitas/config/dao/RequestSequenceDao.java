package com.colt.novitas.config.dao;

import com.colt.novitas.config.entity.RequestId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestSequenceDao extends JpaRepository<RequestId, String>, JpaSpecificationExecutor<RequestId> {
    
    List<RequestId> findByNameAndNovitasServiceId(String name, String novitasServiceId);
    
}
