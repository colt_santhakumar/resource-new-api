/**
 * 
 */
package com.colt.novitas.config.dao;

import java.util.List;

import com.colt.novitas.config.entity.Client;

/**
 * @author omerio
 *
 */
public interface ClientDao {

    List<Client> findAll();
    
    void save(Client client);
}
