/**
 * 
 */
package com.colt.novitas.config.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.colt.novitas.config.entity.Client;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author omerio
 *
 */
@Repository
@Transactional
public class ClientDaoImpl implements ClientDao {

    @PersistenceContext
    private EntityManager entityManager;

    private Session getCurrentSession() {
        return entityManager.unwrap(Session.class);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Client> findAll() {
        Query query = getCurrentSession().createQuery("from Client");
        return (List<Client>) query.list();
    }

    @Override
    public void save(Client client) {
        getCurrentSession().saveOrUpdate(client);
    }
}
