package com.colt.novitas.config.dao.criteria;

import java.util.HashMap;
import java.util.Map;

public enum SearchOperation {

    EQUALITY(":"), NEGATION("!"), GREATER_THAN(">"), GREATER_THAN_OR_EQUAL(">="), LESS_THAN("<"), LESS_THAN_OR_EQUAL("<="), LIKE("~"), STARTS_WITH("*"), ENDS_WITH("#"), IN("IN"), NOT_IN("NOT_IN");

    private static final Map<String, SearchOperation> namesMap = new HashMap<>(7);

    static {
        namesMap.put(EQUALITY.value, EQUALITY);
        namesMap.put(NEGATION.value, NEGATION);
        namesMap.put(GREATER_THAN.value, GREATER_THAN);
        namesMap.put(LESS_THAN.value, LESS_THAN);
        namesMap.put(LIKE.value, LIKE);
        namesMap.put(STARTS_WITH.value, STARTS_WITH);
        namesMap.put(ENDS_WITH.value, ENDS_WITH);
        namesMap.put(IN.value, IN);
        namesMap.put(NOT_IN.value, NOT_IN);
    }

    private final String value;

    SearchOperation(String value) {
        this.value = value;
    }
}
