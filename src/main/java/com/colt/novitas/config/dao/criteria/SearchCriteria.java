package com.colt.novitas.config.dao.criteria;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class SearchCriteria {

    public static final String INNER_OBJECT_DELIMITER = ".";

    private final String key;
    private final SearchOperation operator;
    private final Object value;
}
