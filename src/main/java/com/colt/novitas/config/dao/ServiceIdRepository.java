package com.colt.novitas.config.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.colt.novitas.config.entity.ServiceId;

@Repository
public interface ServiceIdRepository extends JpaRepository<ServiceId, Integer>, ServiceIdRepositoryCustom{
	
}
