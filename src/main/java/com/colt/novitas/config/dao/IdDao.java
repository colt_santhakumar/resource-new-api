package com.colt.novitas.config.dao;

import java.util.List;

import com.colt.novitas.config.entity.RequestId;
import com.colt.novitas.config.entity.ServiceId;

public interface IdDao {
	
	Integer createRequestId(RequestId req);

	void updateRequestId(RequestId req);

	RequestId getRequestIdType(Integer requestId);

	List<RequestId> getTopAllRequestsByRang(int begin,int end);

	String createServiceId(ServiceId req);

	void updateServiceId(ServiceId req);

	ServiceId getServiceIdType(String serviceId);

}
