package com.colt.novitas.config.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.colt.novitas.config.dao.ServiceIdRepositoryCustom;
import com.colt.novitas.config.entity.ServiceId;

@Repository
public class ServiceIdRepositoryImpl implements ServiceIdRepositoryCustom {
	
	@PersistenceContext
	private EntityManager entityManager;

	private Session getCurrentSession() {
		return entityManager.unwrap(Session.class);
	}
	
	@Override
	public String createServiceId(ServiceId req) {
		return String.valueOf(getCurrentSession().save(req));
	}

	@Override
	public void updateServiceId(ServiceId service) {
		Query query = getCurrentSession().createQuery(
				"UPDATE ServiceId id SET id.lastUpdated = :lastUpdated, id.name = :name, id.status = :status WHERE id.id = :serviceId"
						+ " AND (id.lastUpdated <= :lastUpdated OR id.lastUpdated is NULL)");
		query.setDate("lastUpdated", service.getLastUpdated());
		query.setString("name", service.getName());
		query.setString("status", service.getStatus());
		query.setInteger("serviceId", service.getId());
		query.executeUpdate();
	}

	@Override
	public ServiceId getServiceIdType(String serviceId) {
		return  getCurrentSession().get(ServiceId.class,Integer.parseInt(serviceId));
	}

}
