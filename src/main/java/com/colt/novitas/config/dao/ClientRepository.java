package com.colt.novitas.config.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.colt.novitas.config.entity.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {
	
}
