package com.colt.novitas.config.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.colt.novitas.config.entity.Property;

@Repository
public interface PropertyRepository extends JpaRepository<Property, Integer> {

	@Query(name=Property.QUERY_FIND_BY_ITEM_ENVIRONMENT)
	Property findByItemEnvironment(@Param("item") String item, @Param("environment") String environment);
	
}
