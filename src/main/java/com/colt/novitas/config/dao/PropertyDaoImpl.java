/**
 * 
 */
package com.colt.novitas.config.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.colt.novitas.config.entity.Property;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *  *
 */
@Repository
@Transactional
public class PropertyDaoImpl implements PropertyDao {

    @PersistenceContext
    private EntityManager entityManager;

    private Session getCurrentSession() {
        return entityManager.unwrap(Session.class);
    }

    @Override
    public Property findByItemEnvironment(String item, String environment) {
        Query query = getCurrentSession().getNamedQuery(Property.QUERY_FIND_BY_ITEM_ENVIRONMENT);
        query.setString("environment", environment);
        query.setString("item", item);
        return (Property) query.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Property> findAll() {
        Query query = getCurrentSession().createQuery("from Property");
        return (List<Property>) query.list();
    }

    @Override
    public void save(Property property) {
        getCurrentSession().saveOrUpdate(property);
    }
}
