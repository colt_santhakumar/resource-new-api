/**
 * 
 */
package com.colt.novitas.config.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author omerio
 *
 */
@Embeddable
public class PropertyId implements Serializable {

    private static final long serialVersionUID = -7457662495110759678L;

    @Column(name = "environment")
    private String environment;

    @Column(name = "item")
    private String item;

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PropertyId that = (PropertyId) o;
        return Objects.equals(environment, that.environment) &&
                Objects.equals(item, that.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(environment, item);
    }
}
