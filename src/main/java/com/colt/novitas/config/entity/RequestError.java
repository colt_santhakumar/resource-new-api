package com.colt.novitas.config.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "REQUEST_ERROR_DETAILS")
public class RequestError {
	
	@Id
	@Column(name = "ID")
	private String id;
	
	@Column(name = "REQUEST_ID" , nullable = false)
	private Integer requestId;
	
	@Column(name = "STATUS" , nullable = false)
	private String status;
	
	@Column(name = "DEPLOYMENT_ID" , nullable = false)
	private String deploymentId;
	
	@Column(name = "ERROR_STACK")
	@Lob
	private byte[] errorStack;
	
	@Column(name = "PROCESS_INSTANCE_ID", nullable = false)
	private Long processInstanceId;

	@Column(name = "HOST_ADDRESS", nullable = false)
	private String hostAddress;
	
	@Column(name ="TASK_NAME")
    private String taskName;
	
	public Long getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(Long processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}


	public byte[] getErrorStack() {
		return errorStack;
	}

	public void setErrorStack(byte[] errorStack) {
		this.errorStack = errorStack;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ErrorTriageRequest [requestId=" + requestId + ", status=" + status + ", deploymentId=" + deploymentId
				+ ", errorStack=" + errorStack + "]";
	}
	
	

}
