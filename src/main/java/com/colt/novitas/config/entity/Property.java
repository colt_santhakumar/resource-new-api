/**
 * 
 */
package com.colt.novitas.config.entity;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(
                name = Property.QUERY_FIND_BY_ITEM_ENVIRONMENT, 
                query = "select distinct(p) from Property p where p.id.environment = :environment and p.id.item = :item")
})
@Entity
@Table(name = "PROPERTIES")
public class Property {

    public static final String QUERY_FIND_BY_ITEM_ENVIRONMENT = "queryFindPropertyByItemEnvironment";

    @EmbeddedId
    private PropertyId id;

    @Column(name = "value")
    private String value;

    public static Property create(String item, String value, String environment) {
        Property property = new Property();
        property.setValue(value);

        PropertyId id = new PropertyId();
        id.setEnvironment(environment);
        id.setItem(item);
        property.setId(id);

        return property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public PropertyId getId() {
        return id;
    }

    public void setId(PropertyId id) {
        this.id = id;
    }

}
