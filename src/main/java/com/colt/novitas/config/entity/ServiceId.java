package com.colt.novitas.config.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.colt.novitas.config.request.IdType;

import java.util.Date;

@Entity
@Table(name = "SERVICE_SEQUENCE")
public class ServiceId {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "novitas_service_sequence")
	@SequenceGenerator(name = "novitas_service_sequence", sequenceName = "novitas_service_sequence", allocationSize = 1)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Enumerated(EnumType.STRING)
	@Column(name = "TYPE")
	private IdType type;

	@Column(name = "NOVITAS_SERVICE_ID", length = 32)
	private String novitasServiceId;

	@Column(name = "NAME", length = 128)
	private String name;

	@Column(name = "STATUS", length = 16)
	private String status;

	@Column(name = "LAST_UPDATED")
	private Date lastUpdated;

	public ServiceId() {
		super();
	}

	public ServiceId(IdType type) {
		super();
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public IdType getType() {
		return type;
	}

	public void setType(IdType type) {
		this.type = type;
	}

	public String getNovitasServiceId() {
		return novitasServiceId;
	}

	public void setNovitasServiceId(String novitasServiceId) {
		this.novitasServiceId = novitasServiceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
}
