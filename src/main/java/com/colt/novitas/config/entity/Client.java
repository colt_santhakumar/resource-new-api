/**
 * 
 */
package com.colt.novitas.config.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author omerio
 *
 */
@Entity
@Table(name = "CLIENTS")
public class Client {

    @Id
    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "client_name")
    private String name;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
