//old
package com.colt.novitas.config.entity;

import com.colt.novitas.config.request.IdType;
import com.colt.novitas.config.restclients.model.RequestAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "REQUEST_SEQUENCE")
public class RequestId extends ResourceEntity {
  
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="novitas_request_sequence")
	@SequenceGenerator(
		name="novitas_request_sequence",
		sequenceName="novitas_request_sequence",
		allocationSize=1
	)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false)
	private Integer id;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "TYPE")
	private IdType type;

	@Column(name = "NOVITAS_SERVICE_ID", length = 32)
	private String novitasServiceId;

	@Column(name = "NAME", length = 128)
	private String name;

	@Column(name = "STATUS", length = 16)
	private String status;

	@Column(name = "LAST_UPDATED")
	private Date lastUpdated;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "DETAIL_URL")
	private String detailUrl;
	
	@Column(name = "REQUESTED_BY")
	private String requestedBy;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "REQUEST_ACTION")
	private RequestAction requestAction;
	
	@Column(name = "REQUESTED_AT")
	private String requestedAt;
	
	@Column(name = "COMMITMENT_PERIOD")
	private Integer commitmentPeriod;
	
	@Column(name = "CURRENCY")
	private String currency;
	
	@Column(name = "DECOMMISSIONING_CHARGE")
	private Float decommissioningCharge;
	
	@Column(name = "INSTALLATION_CHARGE")
	private Float installationCharge;
	
	@Column(name = "PENALTY_CHARGE")
	private Float penaltyCharge;
	
	@Column(name = "RENTAL_CHARGE")
	private Float rentalCharge;
	
	@Column(name = "RENTAL_UNIT")
	private String rentalUnit;

	@Column(name = "SITE_NAME")
	private String siteName;

	@Column(name = "CSP_KEY")
	private String cspKey;

	@Column(name = "SERVICE_ID")
	private String serviceId;

	@Column(name = "BANDWIDTH")
	private Integer bandwidth;

	public RequestId() {
		super();
	}

	public RequestId(IdType type) {
		super();
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public IdType getType() {
		return type;
	}

	public void setType(IdType type) {
		this.type = type;
	}

	public String getNovitasServiceId() {
		return novitasServiceId;
	}

	public void setNovitasServiceId(String novitasServiceId) {
		this.novitasServiceId = novitasServiceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDetailUrl() {
		return detailUrl;
	}
	
	public void setDetailUrl(String detailUrl) {
		this.detailUrl = detailUrl;
	}
	
	public String getRequestedBy() {
		return requestedBy;
	}
	
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
	
	public RequestAction getRequestAction() {
		return requestAction;
	}
	
	public void setRequestAction(RequestAction requestAction) {
		this.requestAction = requestAction;
	}
	
	public String getRequestedAt() {
		return requestedAt;
	}
	
	public void setRequestedAt(String requestedAt) {
		this.requestedAt = requestedAt;
	}
	
	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}
	
	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}
	
	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}
	
	public Float getInstallationCharge() {
		return installationCharge;
	}
	
	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}
	
	public Float getPenaltyCharge() {
		return penaltyCharge;
	}
	
	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}
	
	public Float getRentalCharge() {
		return rentalCharge;
	}
	
	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}
	
	public String getRentalUnit() {
		return rentalUnit;
	}
	
	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getCspKey() {
		return cspKey;
	}

	public void setCspKey(String cspKey) {
		this.cspKey = cspKey;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}
}
