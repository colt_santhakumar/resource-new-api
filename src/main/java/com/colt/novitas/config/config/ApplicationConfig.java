package com.colt.novitas.config.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.function.BiFunction;

@Configuration
public class ApplicationConfig implements WebMvcConfigurer {

    @Value("${http.connection.timeout.millis}")
    private String httpConnectionTimeout;
    @Value("${http.read.timeout.millis}")
    private String httpReadTimeout;
    
    private static final int CONNECTION_TIMEOUT_DEFAULT = 30_000;
    private static final int READ_TIMEOUT_DEFAULT = 30_000;
    
    @Bean
    public Mapper mapper() {
        return new DozerBeanMapper();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
    
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(clientHttpRequestFactory());
    }
    
    private ClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(getTimeout().apply(httpConnectionTimeout, CONNECTION_TIMEOUT_DEFAULT));
        factory.setReadTimeout(getTimeout().apply(httpReadTimeout, READ_TIMEOUT_DEFAULT));
        HttpClient httpClient = HttpClientBuilder.create()
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();
        factory.setHttpClient(httpClient);
        return factory;
    }
    
    private BiFunction<String, Integer, Integer> getTimeout() {
        return (envValue, defValue) -> {
            int timeout;
            
            try {
                timeout = Integer.parseInt(envValue);
            } catch (NumberFormatException nfe) {
                timeout = defValue;
            }
            
            return timeout;
        };
    }
}
