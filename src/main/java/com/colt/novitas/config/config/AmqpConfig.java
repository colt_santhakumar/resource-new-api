package com.colt.novitas.config.config;

import com.colt.novitas.config.exception.InvalidExchangeMessage;
import com.colt.novitas.config.infrastructure.handlers.MessageBrokerArrivalCallbackHandler;
import com.colt.novitas.config.infrastructure.handlers.MessageQueueArrivalCallbackHandler;
import com.colt.novitas.config.service.ConfigurationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.ConditionalRejectingErrorHandler;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Configuration
@Slf4j
public class AmqpConfig {

    private final static String BROKER_URI = "message.broker.uri";
    private static final String CONFIG_PROFILE = "spring.profiles.active";

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private Environment environment;

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory() throws Exception {
        SimpleRabbitListenerContainerFactory listenerContainerFactory = new SimpleRabbitListenerContainerFactory();
        listenerContainerFactory.setConnectionFactory(connectionFactory());
        listenerContainerFactory.setMessageConverter(jsonMessageConverter());
        listenerContainerFactory.setAcknowledgeMode(AcknowledgeMode.AUTO);
        listenerContainerFactory.setPrefetchCount(1); //only one unacknowledged sent to a consumer

        listenerContainerFactory.setErrorHandler(
                new ConditionalRejectingErrorHandler(
                        new InvalidExchangeMessage()
                ));

        return listenerContainerFactory;
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public ConnectionFactory connectionFactory() throws URISyntaxException {

        String env = environment.getProperty(CONFIG_PROFILE);

        CachingConnectionFactory connectionFactory =
                new CachingConnectionFactory(new URI(configurationService.getValue(env, BROKER_URI)));

        connectionFactory.setRequestedHeartBeat(60);

        connectionFactory.setCacheMode(CachingConnectionFactory.CacheMode.CHANNEL);
        connectionFactory.setChannelCacheSize(100);

        connectionFactory.setPublisherConfirms(true);

        connectionFactory.setPublisherReturns(true);

        return connectionFactory;
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {

        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());

        rabbitTemplate.setConfirmCallback(getMessageBrokerArrivalCallback());

        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setReturnCallback(getMessageQueueArrivalCallback());

        rabbitTemplate.setRetryTemplate(retryTemplate());

        return rabbitTemplate;
    }

    @Bean
    public RetryTemplate retryTemplate() {
        RetryTemplate retryTemplate = new RetryTemplate();

        FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
        fixedBackOffPolicy.setBackOffPeriod(6000l);
        retryTemplate.setBackOffPolicy(fixedBackOffPolicy);

        SimpleRetryPolicy simpleRetryPolicy = new SimpleRetryPolicy();
        simpleRetryPolicy.setMaxAttempts(5);

        retryTemplate.setRetryPolicy(simpleRetryPolicy);

        return retryTemplate;
    }

    @Bean
    public RabbitTemplate.ConfirmCallback getMessageBrokerArrivalCallback() {
        return new MessageBrokerArrivalCallbackHandler();
    }

    @Bean
    public RabbitTemplate.ReturnCallback getMessageQueueArrivalCallback() {
        return new MessageQueueArrivalCallbackHandler();
    }
}
