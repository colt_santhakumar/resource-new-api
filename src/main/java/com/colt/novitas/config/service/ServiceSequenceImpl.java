package com.colt.novitas.config.service;

import com.colt.novitas.config.dao.ServiceSequenceDao;
import com.colt.novitas.config.entity.ServiceId;
import com.colt.novitas.config.entity.ServiceIdManual;
import com.colt.novitas.config.request.CreateServiceRequest;
import com.colt.novitas.config.request.IdType;
import com.colt.novitas.config.util.ObjectConverter;
import com.colt.novitas.config.util.URLParamHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ServiceSequenceImpl implements ServiceSequence {

    @Autowired
    private ObjectConverter converter;

    @Autowired
    private ServiceSequenceDao serviceSequenceDao;

    @PersistenceContext
    private EntityManager entityManager;



    @Override
    public List<com.colt.novitas.config.request.Service> getServicesByNameAndNovitasServiceId(String name, String novitasServiceId) {
        String decodedName = URLParamHelper.decodeParams(name);
        List<ServiceId> serviceIds = serviceSequenceDao.findByNameAndNovitasServiceId(decodedName, novitasServiceId);

        return converter.convert(serviceIds, com.colt.novitas.config.request.Service.class);
    }

    @Override
    public List<com.colt.novitas.config.request.Service> getServicesByServiceIds(List<String> serviceIds) {
        List<Integer> idsInt = serviceIds.stream().map(Integer::parseInt).collect(Collectors.toList());
        List<ServiceId> ids = serviceSequenceDao.findAllByServiceIds(idsInt);

        return converter.convert(ids, com.colt.novitas.config.request.Service.class);
    }

    @Override
    @Transactional
    public void createServiceManual(CreateServiceRequest createServiceRequest) {
        ServiceIdManual service = new ServiceIdManual();
        service.setId(Integer.parseInt(createServiceRequest.getId()));
        service.setNovitasServiceId(createServiceRequest.getParent_service_id());
        service.setName(createServiceRequest.getName());
        service.setStatus(createServiceRequest.getStatus().name());
        service.setLastUpdated(new Date());
        service.setType(IdType.PORT);
        entityManager.persist(service);
    }
}
