package com.colt.novitas.config.service;

import com.colt.novitas.config.request.CreateServiceRequest;
import com.colt.novitas.config.request.Service;

import java.util.List;

public interface ServiceSequence {

    List<Service> getServicesByNameAndNovitasServiceId(String name, String novitasServiceId);

    List<Service> getServicesByServiceIds(List<String> novitasServiceIds);

    void createServiceManual(CreateServiceRequest createServiceRequest);
}
