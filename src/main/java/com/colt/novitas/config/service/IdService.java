package com.colt.novitas.config.service;

import com.colt.novitas.config.infrastructure.pojos.RequestChangeEventNotification;
import com.colt.novitas.config.infrastructure.pojos.ResourceChangeMessage;
import com.colt.novitas.config.request.IdType;

import java.util.Map;
import java.util.Optional;

public interface IdService {

	Optional<Integer> createRequestId(IdType type);

	void updateRequestId(ResourceChangeMessage resourceChangeMessage);
	
	void updateRequestWithRequestChangeEvent(RequestChangeEventNotification requestChangeEventNotification);

	Optional<IdType> getRequestIdType(Integer requestId);
	
	Map<Integer,IdType> getRequestIds(int beginAt,int endAt);

	Optional<String> createServiceId(IdType type);

	void updateServiceId(ResourceChangeMessage resourceChangeMessage);

	Optional<IdType> getServiceIdType(String serviceId);

}
