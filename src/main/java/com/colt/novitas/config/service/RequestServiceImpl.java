package com.colt.novitas.config.service;

import com.colt.novitas.config.dao.RequestSequenceDao;
import com.colt.novitas.config.dao.criteria.RequestQueryParam;
import com.colt.novitas.config.dao.criteria.SearchCriteria;
import com.colt.novitas.config.dao.criteria.SearchOperation;
import com.colt.novitas.config.dao.criteria.SpecificationBuilder;
import com.colt.novitas.config.entity.RequestId;
import com.colt.novitas.config.request.IdType;
import com.colt.novitas.config.request.Request;
import com.colt.novitas.config.util.DateFormatUtils;
import com.colt.novitas.config.util.ObjectConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
@Slf4j
public class RequestServiceImpl implements RequestService {

    @Autowired
    private ObjectConverter converter;

    @Autowired
    private RequestSequenceDao requestSequenceDao;

    @Override
    public List<Request> getRequestsBySearchCriteria(String name, String novitasServiceId, List<String> status, String type) {
    
        SpecificationBuilder<RequestId> builder = new SpecificationBuilder<>();
        List<SearchCriteria> searchCriteriaList = new ArrayList<>();
        
        searchCriteriaList.add(new SearchCriteria(RequestQueryParam.NOVITAS_SERVICE_ID.getName(), SearchOperation.EQUALITY, novitasServiceId));
        
        if(StringUtils.isNotEmpty(name)) {
            searchCriteriaList.add(new SearchCriteria(RequestQueryParam.NAME.getName(), SearchOperation.EQUALITY, name));
        }
        if(StringUtils.isNotEmpty(type)) {
            searchCriteriaList.add(new SearchCriteria(RequestQueryParam.TYPE.getName(), SearchOperation.EQUALITY, IdType.valueOf(type)));
        }
        if(null != status && status.size() > 0) {
            searchCriteriaList.add(new SearchCriteria(RequestQueryParam.STATUS.getName(), SearchOperation.IN, status));
        }
        
        Specification<RequestId> specification = builder.getResourceQuerySpecification(searchCriteriaList);

        List<RequestId> requests = requestSequenceDao.findAll(specification);

        return requests
                .stream()
                .map(requestId -> {
                    Request request = new Request();
                    request.setId(requestId.getId());
                    request.setName(requestId.getName());
                    if (Objects.nonNull(requestId.getType())) {
                        request.setType(requestId.getType().name());
                    }
                    request.setNovitasServiceId(requestId.getNovitasServiceId());
                    request.setStatus(requestId.getStatus());
                    request.setLastUpdated(DateFormatUtils.formatDate(requestId.getLastUpdated()));
                    request.setDescription(requestId.getDescription());
                    request.setDetailUrl(requestId.getDetailUrl());
                    request.setRequestedBy(requestId.getRequestedBy());
                    request.setAction(requestId.getRequestAction());
                    request.setRequestedAt(requestId.getRequestedAt());
                    request.setCommitmentPeriod(requestId.getCommitmentPeriod());
                    request.setCurrency(requestId.getCurrency());
                    request.setDecommissioningCharge(requestId.getDecommissioningCharge());
                    request.setInstallationCharge(requestId.getInstallationCharge());
                    request.setPenaltyCharge(requestId.getPenaltyCharge());
                    request.setRentalCharge(requestId.getRentalCharge());
                    request.setRentalUnit(requestId.getRentalUnit());
                    request.setServiceId(requestId.getServiceId());
                    request.setBandwidth(requestId.getBandwidth());
                    request.setSiteName(requestId.getSiteName());
                    request.setCspKey(requestId.getCspKey());
                    return request;
                }).collect(toList());
    }
}
