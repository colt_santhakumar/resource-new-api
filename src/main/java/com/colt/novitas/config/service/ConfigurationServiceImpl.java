/**
 * 
 */
package com.colt.novitas.config.service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.PostConstruct;

import com.colt.novitas.config.infrastructure.ConfigurationChangesNotifier;
import com.colt.novitas.config.controller.Response;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.web.util.matcher.IpAddressMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.colt.novitas.config.dao.ClientRepository;
import com.colt.novitas.config.dao.PropertyRepository;
import com.colt.novitas.config.entity.Client;
import com.colt.novitas.config.entity.Property;
import com.colt.novitas.config.exception.UnauthorisedException;

/**
 * @author omerio
 *
 */
@Service
public class ConfigurationServiceImpl implements ConfigurationService {

    private static final Logger LOG = Logger.getLogger(ConfigurationServiceImpl.class);

    private static final String SEP = "::";

    @Autowired
    private PropertyRepository propertyRepo;

    @Autowired
    private ClientRepository clientDao;

    @Lazy
    @Autowired
    private ConfigurationChangesNotifier configurationChangesNotifier;

    private Map<String, String> properties = new HashMap<>();

    private CopyOnWriteArrayList<String> whitelistIps = new CopyOnWriteArrayList<>();

    private ReadWriteLock lock = new ReentrantReadWriteLock();

    /**
     * Load all the values from the database and cache them
     */
    @PostConstruct
    protected void refresh() {

        LOG.info("Refreshing properties and IP addresses caches");

        List<Property> props = propertyRepo.findAll();

        lock.writeLock().lock();

        // clear the caches
        properties.clear();
        whitelistIps.clear();

        try {
            updatePropertiesCache(props);
        } finally {
            lock.writeLock().unlock();
        }

        updateWhiteListIpsCache();
    }

    private void updatePropertiesCache(List<Property> props) {
        for (Property property : props) {
            String key = this.getKey(property.getId().getEnvironment(), property.getId().getItem());
            properties.put(key, property.getValue());
        }
    }

    private void updateWhiteListIpsCache() {
        List<Client> clients = clientDao.findAll();

        for (Client client : clients) {
            whitelistIps.add(client.getIpAddress());
        }
    }

    @Override
    public void reload() {
        refresh();
        configurationChangesNotifier.sendConfigurationChangesNotification(new HashMap<>(properties));
    }

    @Override
    public List<Response> getItemsByEnvironment(String environment, String ipAddress) throws UnauthorisedException {

        Validate.notEmpty(environment);
        Validate.notEmpty(ipAddress);
        validateAccess(ipAddress);

        lock.readLock().lock();
        try {
            return getItemsFromCache(environment);
        } finally {
            lock.readLock().unlock();
        }
    }

    private List<Response> getItemsFromCache(String environment) {
        List<Response> items = new ArrayList<>();
        properties.forEach(
                (key, value) -> {
                    if (isSameEnvironment(key, environment)) {
                        items.add(new Response(environment, getGenuineKey(key), value));
                    }
                }
        );
        return items;
    }

    private boolean isSameEnvironment(String composedKey, String environment) {
        return Objects.equals(getEnvironmentFromKey(composedKey), environment);
    }

    private String getEnvironmentFromKey(String composedKey) {
        return composedKey.split(SEP)[0];
    }

    private String getGenuineKey(String composedKey) {
        return composedKey.split(SEP)[1];
    }

    @Override
    public String getValue(String environment, String item, String ipAddress) throws UnauthorisedException {
        // validate parameters
        Validate.notEmpty(item);
        Validate.notEmpty(environment);
        Validate.notEmpty(ipAddress);

        // is the IP address allowed?
        validateAccess(ipAddress);

        return getValue(environment, item);
    }

    @Override
    public String getValue(String environment, String item) {
        LOG.debug("Retrieving configuration for item: " + item + ", environment: " + environment);
        return getOrReloadValue(environment, item);
    }

    private void validateAccess(String ipAddress) throws UnauthorisedException {
        // is the IP address allowed?
        if (!whitelistIps.contains(ipAddress) && !isIpPartOfASubnetMask(ipAddress)) {
            
            List<Client> clients = this.clientDao.findAll();
            
            for (Client client : clients) {
                whitelistIps.add(client.getIpAddress());
            }
            
            if (!whitelistIps.contains(ipAddress) && !isIpPartOfASubnetMask(ipAddress)) {
                throw new UnauthorisedException();
            }
        }
    }

    private boolean isIpPartOfASubnetMask(String ipAddress) {
        return Objects.nonNull(getSubnetMaskMatchingIp(ipAddress));
    }

    private String getSubnetMaskMatchingIp(String ipAddress) {
        return whitelistIps
                .stream()
                .filter(this::isPrivateManagedIps)
                .filter(whitelistIp -> ipMatchesSubnetMask(whitelistIp, ipAddress))
                .findFirst()
                .orElse(null);
    }

    private boolean isPrivateManagedIps(String ipAddress) {
        String[] addressAndMask = StringUtils.split(ipAddress, "/");

        if (Objects.nonNull(addressAndMask) && addressAndMask.length > 1) {
            try {
                Integer.parseInt(addressAndMask[1]);
                return true;
            } catch (NumberFormatException nfe) {
                return false;
            }
        }

        return false;
    }

    private boolean ipMatchesSubnetMask(String subnetMask, String ipAddress) {

        boolean matches = false;

        try {
            matches = new IpAddressMatcher(subnetMask).matches(ipAddress);
        } catch (IllegalArgumentException e) {
            LOG.warn("Error occurred when matching ipAddress: " + ipAddress + " with subnet mask: " + subnetMask + ". Cause: " + e.getMessage());
        }

        return matches;
    }

    /**
     * Return a key to use for the cache hashmap
     * 
     * @param item
     * @param environment
     * @return
     */
    private String getKey(String environment, String item) {
        // Java 5+ the compiler will optimize these to use StringBuilder anyway
        return environment + SEP + item;
    }

    private String getOrReloadValue(String environment, String item) {
        String key = this.getKey(environment, item);

        String value = getItemFromCache(key);

        // not found the cache, check the database
        if (value == null) {
            value = getItemAndUpdateCache(key, item, environment);
        }

        return value;
    }

    private String getItemFromCache(String key) {
        String value;

        lock.readLock().lock();
        try {
            value = this.properties.get(key);
        } finally {
            lock.readLock().unlock();
        }

        return value;
    }

    private String getItemAndUpdateCache(String key, String item, String environment) {
        Property property = this.propertyRepo.findByItemEnvironment(item, environment);

        String value = null;
        if (property != null) {
            // found in the database, now update the cache
            lock.writeLock().lock();
            try {
                value = property.getValue();
                this.properties.put(key, value);
            } finally {
                lock.writeLock().unlock();
            }
        }

        return value;
    }
}
