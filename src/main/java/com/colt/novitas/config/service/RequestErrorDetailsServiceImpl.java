package com.colt.novitas.config.service;

import java.util.Optional;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.colt.novitas.config.dao.RequestErrorDetailsDao;
import com.colt.novitas.config.entity.RequestError;
import com.colt.novitas.config.request.RequestErrorDetailsRequest;

@Service
@Transactional
public class RequestErrorDetailsServiceImpl implements RequestErrorDetailsService {

	@Autowired
	private RequestErrorDetailsDao erroDao;

	@Autowired
	private Mapper mapper;

	@Override
	public RequestErrorDetailsRequest getRequestErrors(Integer requestId) {
		return erroDao.getRequestErrors(requestId).map(data -> mapper.map(data, RequestErrorDetailsRequest.class))
				.orElseGet(() -> null);
	}

	@Override
	public void save(RequestErrorDetailsRequest req) {
		req.setStatus("PENDING");
		erroDao.save(mapper.map(req, RequestError.class));
	}

	@Override
	public void update(RequestErrorDetailsRequest req) {
		Optional<RequestError> data = erroDao.getRequestErrors(req.getRequestId());
		if(data.isPresent()){
			RequestError r = data.get();
			r.setStatus(req.getStatus());
		 erroDao.update(r);
		}
	}
}
