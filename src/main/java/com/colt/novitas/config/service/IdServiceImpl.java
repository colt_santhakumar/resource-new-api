package com.colt.novitas.config.service;

import com.colt.novitas.config.dao.RequestIdRepository;
import com.colt.novitas.config.dao.ServiceIdRepository;
import com.colt.novitas.config.entity.RequestId;
import com.colt.novitas.config.entity.ServiceId;
import com.colt.novitas.config.infrastructure.pojos.RequestChangeEventNotification;
import com.colt.novitas.config.infrastructure.pojos.ResourceChangeMessage;
import com.colt.novitas.config.request.IdType;
import com.colt.novitas.config.restclients.ApiClient;
import com.colt.novitas.config.restclients.model.RequestAction;
import com.colt.novitas.config.restclients.model.RequestCommonAttributes;
import com.colt.novitas.config.util.DateFormatUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class IdServiceImpl implements IdService {

	@Autowired
	private RequestIdRepository requestIdRepository;
	
	@Autowired
	private ServiceIdRepository serviceIdRepository;
	
	@Autowired
	private ApiClient apiClient;

	@Override
	public Optional<Integer> createRequestId(IdType type) {
		RequestId req = new RequestId(type);
		return Optional.ofNullable(requestIdRepository.createRequestId(req));
	}

	public void updateRequestId(ResourceChangeMessage resourceChangeMessage) {
		
		RequestId requestId = requestIdRepository.getRequestIdType(Integer.valueOf(resourceChangeMessage.getResourceId()));
		
		requestId.setNovitasServiceId(resourceChangeMessage.getNovitasServiceId());
		
		if (StringUtils.isNotEmpty(resourceChangeMessage.getNewName())) {
			requestId.setName(resourceChangeMessage.getNewName());
		}

		if (resourceChangeMessage.getStatus() != null) {
			requestId.setStatus(resourceChangeMessage.getStatus());
		}

		requestId.setLastUpdated(resourceChangeMessage.getTimestamp());

		requestIdRepository.updateRequestId(requestId);
	}
	
	@Override
	public void updateRequestWithRequestChangeEvent(RequestChangeEventNotification eventNotification) {
		
		log.info("Event notification: {}", eventNotification);
		RequestId requestId = requestIdRepository.getRequestIdType(Integer.valueOf(eventNotification.getResourceId()));
		
		RequestCommonAttributes requestResponse = apiClient.getRequestByUrl(eventNotification);
		
		if (null != requestResponse) {
			
			if (StringUtils.isNotEmpty(eventNotification.getResourceUrl())) {
				requestId.setDetailUrl(eventNotification.getResourceUrl());
			}

            if (StringUtils.isNotEmpty(requestResponse.getNovitasServiceId())) {
                requestId.setNovitasServiceId(requestResponse.getNovitasServiceId());
            }

            if (StringUtils.isNotEmpty(requestResponse.getName())) {
                requestId.setName(requestResponse.getName());
            }

            if (requestResponse.getStatus() != null) {
                requestId.setStatus(requestResponse.getStatus());
            }

            if (StringUtils.isNotEmpty(requestResponse.getDescription())) {
                requestId.setDescription(requestResponse.getDescription());
            }

            if (StringUtils.isNotEmpty(requestResponse.getRequestedBy())) {
                requestId.setRequestedBy(requestResponse.getRequestedBy());
            }

            if (StringUtils.isNotEmpty(requestResponse.getAction())) {
                requestId.setRequestAction(RequestAction.valueOf(requestResponse.getAction()));
            }

            if (StringUtils.isNotEmpty(requestResponse.getRequestedAt())) {
                requestId.setRequestedAt(requestResponse.getRequestedAt());
            }

            if (StringUtils.isNotEmpty(requestResponse.getCurrency())) {
                requestId.setCurrency(requestResponse.getCurrency());
            }

            if ( null != requestResponse.getCommitmentPeriod()) {
                requestId.setCommitmentPeriod(requestResponse.getCommitmentPeriod());
            }

            if ( null != requestResponse.getDecommissioningCharge()) {
                requestId.setDecommissioningCharge(requestResponse.getDecommissioningCharge());
            }

            if ( null != requestResponse.getInstallationCharge()) {
                requestId.setInstallationCharge(requestResponse.getInstallationCharge());
            }

            if ( null != requestResponse.getPenaltyCharge()) {
                requestId.setPenaltyCharge(requestResponse.getPenaltyCharge());
            }

            if ( null != requestResponse.getRentalCharge()) {
                requestId.setRentalCharge(requestResponse.getRentalCharge());
            }

            if (StringUtils.isNotEmpty(requestResponse.getRentalUnit())) {
                requestId.setRentalUnit(requestResponse.getRentalUnit());
            }

            if (Objects.nonNull(requestResponse.getLastUpdated())) {
                requestId.setLastUpdated(DateFormatUtils.formatLDTString(requestResponse.getLastUpdated()));
            }

            if (StringUtils.isNotEmpty(requestResponse.getServiceId())) {
                requestId.setServiceId(requestResponse.getServiceId());
            }

            if (Objects.nonNull(requestResponse.getBandwidth())) {
                requestId.setBandwidth(requestResponse.getBandwidth());
            }

			if (StringUtils.isNotEmpty(requestResponse.getCspKey())) {
				requestId.setCspKey(requestResponse.getCspKey());
			}

			if (StringUtils.isNotEmpty(requestResponse.getSiteName())) {
				requestId.setSiteName(requestResponse.getSiteName());
			}
			
			log.info("Update latest request details post notification: {}", requestResponse);
			requestIdRepository.updateRequestId(requestId);
			log.info("Request change event update success");
		}
		else {
			log.error("Request Change notification failed, because request id : {} not found", eventNotification.getResourceId());
		}
	}
	
	@Override
	public Optional<IdType> getRequestIdType(Integer requestId) {
		return Optional.ofNullable(requestIdRepository.getRequestIdType(requestId)).map(id -> Optional.ofNullable(id.getType()))
				.orElse(Optional.empty());
	}
	
	@Override
	public Map<Integer,IdType> getRequestIds(int beginAt,int endAt) {
		List<RequestId> requests = requestIdRepository.getTopAllRequestsByRang(beginAt, endAt);
		return requests.stream()
				.filter(id -> id.getType() != null)
				.collect(Collectors.toMap(RequestId::getId, RequestId::getType));		
	}

	@Override
	public Optional<String> createServiceId(IdType type) {
		ServiceId service = new ServiceId(type);
		return Optional.ofNullable(serviceIdRepository.createServiceId(service));
	}

	@Override
	public void updateServiceId(ResourceChangeMessage resourceChangeMessage) {
		ServiceId serviceId = serviceIdRepository.getServiceIdType(resourceChangeMessage.getResourceId());
		
		serviceId.setNovitasServiceId(resourceChangeMessage.getNovitasServiceId());

		if (StringUtils.isNotEmpty(resourceChangeMessage.getNewName())) {
			serviceId.setName(resourceChangeMessage.getNewName());
		}

		if (resourceChangeMessage.getStatus() != null) {
			serviceId.setStatus(resourceChangeMessage.getStatus());
		}

		serviceId.setLastUpdated(resourceChangeMessage.getTimestamp());

		serviceIdRepository.updateServiceId(serviceId);
	}

	@Override
	public Optional<IdType> getServiceIdType(String serviceId) {
		return Optional.ofNullable(serviceIdRepository.getServiceIdType(serviceId)).map(id -> Optional.ofNullable(id.getType()))
				.orElse(Optional.empty());
	}
}
