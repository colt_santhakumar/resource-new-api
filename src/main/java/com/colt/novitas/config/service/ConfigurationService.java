/**
 * 
 */
package com.colt.novitas.config.service;

import com.colt.novitas.config.controller.Response;
import com.colt.novitas.config.exception.UnauthorisedException;

import java.util.List;

public interface ConfigurationService {

    /**
     * Get a property value for the provided environment and item
     * 
     * @param environment
     * @param item
     * @param ipAddress
     * @return
     * @throws UnauthorisedException
     *             if the ipAddress is not authorised
     */
    String getValue(String environment, String item, String ipAddress) throws UnauthorisedException;

    /**
     * Get property value for the provided environment.
     * Used for internal purposes of the api only
     */
    String getValue(String environment, String item);

    /**
     * Refresh the configurations from the database.
     */
    void reload();

    /**
     * Validate the caller IP
     *     &&
     * Return all properties for a specific environment
     * @return
     * @throws UnauthorisedException
     */
    List<Response> getItemsByEnvironment(String environment, String ipAddress) throws UnauthorisedException;
}
