package com.colt.novitas.config.service;

import com.colt.novitas.config.request.Request;

import java.util.List;

public interface RequestService {

    List<Request> getRequestsBySearchCriteria(String name, String novitasServiceId, List<String> status, String type);
}
