package com.colt.novitas.config.service;

import com.colt.novitas.config.request.RequestErrorDetailsRequest;

public interface RequestErrorDetailsService {

	public RequestErrorDetailsRequest getRequestErrors(Integer requestId);

	public void save(RequestErrorDetailsRequest req);

	public void update(RequestErrorDetailsRequest req);

}
