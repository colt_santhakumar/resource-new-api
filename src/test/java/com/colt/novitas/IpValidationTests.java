package com.colt.novitas;

import com.colt.novitas.test.category.UnitTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.security.web.util.matcher.IpAddressMatcher;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Category(value = UnitTest.class)
public class IpValidationTests {

    private static final String RANCHER_SUBNET = "10.42.0.0/16";

    @Test
    public void ipIncludedInSubnet_Success() {
        IpAddressMatcher ipAddressMatcher = new IpAddressMatcher(RANCHER_SUBNET);

        List<String> ipAddresses = Arrays.asList("10.42.1.38", "10.42.1.255", "10.42.255.255", "10.42.0.1");
        for (String ipAddress : ipAddresses) {
            assertTrue(ipAddressMatcher.matches(ipAddress));
        }
    }

    @Test
    public void ipIncludedInSubnet_Failed() {
        IpAddressMatcher ipAddressMatcher = new IpAddressMatcher(RANCHER_SUBNET);

        List<String> ipAddresses = Arrays.asList("127.0.0.1", "172.17.0.1", "10.43.0.1");
        for (String ipAddress : ipAddresses) {
            assertFalse(ipAddressMatcher.matches(ipAddress));
        }
    }
}
