/**
 * 
 */
package com.colt.novitas.config.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.colt.novitas.config.entity.Property;
import com.colt.novitas.test.category.UnitTest;

/**
 * @author omerio
 *
 */

@Category(UnitTest.class)
@RunWith(SpringRunner.class)
@DataJpaTest(showSql = false)
@TestPropertySource(
        properties = {
                "spring.jpa.hibernate.ddl-auto=update",
                "spring.jpa.generate-ddl=true",
                "spring.jpa.database-platform=org.hibernate.dialect.H2Dialect"
        }
)
public class PropertyDaoTest {

    private static final String ENV = "DEV";

	private static final String WRONG_ENV  = null;

    @Autowired
    private PropertyRepository propertyRepository;

    Properties props = new Properties();
    Map<String, String> itemsValues = new HashMap<String, String>();

    /**
     * Read from a property file and insert into the database
     * 
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        InputStream stream = PropertyDaoTest.class.getResourceAsStream("/test.properties");

        props.load(stream);
        
        assertNotNull(propertyRepository);

        for (Enumeration<Object> e = props.keys(); e.hasMoreElements();) {

            String key = (String) e.nextElement();
            System.out.println(key);
            String value = props.getProperty(key);

            Property property = Property.create(key, value, ENV);

            propertyRepository.save(property);

            itemsValues.put(key, value);

        }

    }

    /**
     * Test method for
     * {@link com.colt.novitas.config.dao.PropertyDaoImpl#findByItemEnvironment(java.lang.String, java.lang.String)}
     * .
     */
    @Test
    public void testFindByItemAndEnvironment() {

    	String firstitem = itemsValues.keySet().stream().findFirst().get();
        Property property = propertyRepository.findByItemEnvironment(firstitem, ENV);
            
        assertNotNull(property);
            
        assertEquals(itemsValues.get(firstitem), property.getValue());
            
    }
    
    @Test
    public void testFindByItemButNullEnvironment() {
       
    		String firstitem = itemsValues.keySet().stream().findFirst().get();
			Property property = propertyRepository.findByItemEnvironment(firstitem, null);
            
            assertNull(property);   
        
    }
   
    @Test
    public void testFindByEnvironmentButNoItem() {
        	
            Property property = propertyRepository.findByItemEnvironment(null, ENV);
            
            assertNull(property);
            
    }
    
    @Test
    public void testFindByNullEnvironmentNullItem() {
        	
            Property property = propertyRepository.findByItemEnvironment(null, null);
            
            assertNull(property);
          
    }
    
    @Test
    public void testFindByUnauthEnvironment() {
        	
    		String firstitem = itemsValues.keySet().stream().findFirst().get();
    		Property property = propertyRepository.findByItemEnvironment(firstitem, WRONG_ENV );
        
    		assertNull(property);   
          
    }
    
    

    /**
     * Test method for
     * {@link com.colt.novitas.config.dao.PropertyDaoImpl#findAll()}.
     */
    @Test
    public void testFindAll() {
    	
        List<Property> properties = propertyRepository.findAll();

        assertEquals(itemsValues.size(), properties.size());
    }
    
    
}
