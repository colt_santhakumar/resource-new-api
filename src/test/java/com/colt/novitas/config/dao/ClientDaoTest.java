
package com.colt.novitas.config.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.colt.novitas.config.entity.Client;
import com.colt.novitas.test.category.UnitTest;

@Category(UnitTest.class)
@RunWith(SpringRunner.class)
@DataJpaTest(showSql = false)
@TestPropertySource(
        properties = {
                "spring.jpa.hibernate.ddl-auto=update",
                "spring.jpa.generate-ddl=true",
                "spring.jpa.database-platform=org.hibernate.dialect.H2Dialect"
        }
)
public class ClientDaoTest {

	@Autowired
    ClientRepository clientRepo;
	
	
	  @Before public void setUp() throws Exception{
	  
	  Client client = new Client(); client.setIpAddress("1.2.3.4");
	  client.setName("client_demo");
	  
	  clientRepo.save(client);
	  
	  }
	  
	  
	  @Test public void testFindAll() {
	  
	  java.util.List<Client> allClientsFound = clientRepo.findAll();
	  
	  assertEquals(1, allClientsFound.size());
	  
	  }
	 
}
