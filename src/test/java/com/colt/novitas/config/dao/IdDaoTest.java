package com.colt.novitas.config.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.hibernate.exception.DataException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.colt.novitas.config.entity.RequestId;
import com.colt.novitas.config.entity.ServiceId;
import com.colt.novitas.config.request.IdType;
import com.colt.novitas.test.category.UnitTest;

@Category(UnitTest.class)
@RunWith(SpringRunner.class)
@DataJpaTest(showSql = false)
@TestPropertySource(
        properties = {
                "spring.jpa.hibernate.ddl-auto=update",
                "spring.jpa.generate-ddl=true",
                "spring.jpa.database-platform=org.hibernate.dialect.H2Dialect"
        }
)
public class IdDaoTest {

	@Autowired
    RequestIdRepository requestIdRepository;
	
	@Autowired
    ServiceIdRepository serviceIdRepository;
	
	int myrequest;
	String myservice;
	
	@Before
	public void setUp() throws Exception{
		
		RequestId requestid = new RequestId();
		
		requestid.setType(IdType.PORT);
        requestid.setName("name_demo");
        requestid.setStatus("stat_demo");  
        
        myrequest = requestIdRepository.createRequestId(requestid);

        ServiceId serviceid = new ServiceId();
       
		serviceid.setType(IdType.CONNECTION);
        serviceid.setName("serv_demo");
        serviceid.setStatus("ser_stauts_demo");  
        
        myservice = serviceIdRepository.createServiceId(serviceid);
        
        
	}

	@Test
	public void testUpdateRequestId() {
		
		RequestId requestid_B = requestIdRepository.getRequestIdType(myrequest);
		
        requestid_B.setName("nameB_demo");
     
		requestIdRepository.updateRequestId(requestid_B);
		RequestId result = requestIdRepository.getRequestIdType(myrequest);
		
		assertEquals(requestid_B.getId() , result.getId());
		assertEquals(requestid_B.getName() , result.getName());
		assertEquals(requestid_B.getStatus(), result.getStatus());
		
		
	}
	

	
	//(expected = DataIntegrityViolationException.class)
	@Test
	public void testUpdateRequestId_NameLongerthan128() {
		
		RequestId requestid_D = requestIdRepository.getRequestIdType(myrequest);
		
        requestid_D.setName("requestidb_demoooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo");
        //requestid_D.setLastUpdated();
        
        
        assertThrows(DataIntegrityViolationException.class, () -> requestIdRepository.updateRequestId(requestid_D));

        //requestIdRepository.updateRequestId(requestid_D);
		
	}
	
	@Test
	public void testUpdateRequestId_StatusLongerthan16() {
		
		RequestId requestid_E = requestIdRepository.getRequestIdType(myrequest);
		
        requestid_E.setStatus("stat_demooooooooooooooooooooooo");
        
        assertThrows(DataIntegrityViolationException.class, () -> requestIdRepository.updateRequestId(requestid_E));

		//requestIdRepository.updateRequestId(requestid_E);
		
	}
	
	
	@Test
	public void testUpdateServiceId() {
		
		ServiceId serviceid_A = serviceIdRepository.getServiceIdType(myservice);
		
		serviceid_A.setType(IdType.PORT);
        serviceid_A.setName("nameB_demo");     
        serviceid_A.setStatus("statB_demo");
        	
        serviceIdRepository.updateServiceId(serviceid_A);
		ServiceId result = serviceIdRepository.getServiceIdType(myservice);
		
		assertEquals(serviceid_A.getId() , result.getId());
		assertEquals(serviceid_A.getName() , result.getName());
		assertEquals(serviceid_A.getStatus(), result.getStatus());
		
	}

	
	
	@Test
	public void testUpdateServiceId_NameLongerthan128() {
		
		ServiceId serviceid_D = serviceIdRepository.getServiceIdType(myservice);
		
        serviceid_D.setName("requestidb_demoooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo");
        
        assertThrows(DataIntegrityViolationException.class, () -> serviceIdRepository.updateServiceId(serviceid_D));
        
        //serviceIdRepository.updateServiceId(serviceid_D);
		
	}
	
	@Test
	public void testUpdateServiceId_StatusLongerthan16() {
		
		ServiceId serviceid_E = serviceIdRepository.getServiceIdType(myservice);
		
        serviceid_E.setStatus("stat_demooooooooooooooooooooooo");
        
        assertThrows(DataIntegrityViolationException.class, () -> serviceIdRepository.updateServiceId(serviceid_E));

        //serviceIdRepository.updateServiceId(serviceid_E);
		
	}
	


}
