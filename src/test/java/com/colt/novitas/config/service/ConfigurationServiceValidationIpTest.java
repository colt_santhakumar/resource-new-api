package com.colt.novitas.config.service;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.colt.novitas.config.dao.ClientRepository;
import com.colt.novitas.config.dao.PropertyRepository;
import com.colt.novitas.config.entity.Client;
import com.colt.novitas.config.entity.Property;
import com.colt.novitas.config.entity.PropertyId;
import com.colt.novitas.config.exception.UnauthorisedException;

@RunWith(MockitoJUnitRunner.class)
public class ConfigurationServiceValidationIpTest {

    @InjectMocks
    private ConfigurationServiceImpl service;
    @Mock
    private ClientRepository clientRepository;
    @Mock
    private PropertyRepository propertyRepository;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private static final String DORIAN_HOME_IP_ADDRESS = "10.227.129.170";
    private static final String DORIAN_WORK_IP_ADDRESS = "10.224.13.146";
    private static final String RANCHER_SUBNET_IP_ADDRESSES = "10.42.0.0/16";

    private static final String PROPERTY_ITEM_1 = "serviceinventory.baseurl";
    private static final String PROPERTY_ITEM_2 = "resource.new.api.baseurl";
    private static final String PROPERTY_ITEM_3 = "serviceinventory.username";

    private static final String PROPERTY_VALUE_1 = "http://srv-colt-05.ebs.msft:8077/service-inventory/api";
    private static final String PROPERTY_VALUE_2 = "http://srv-colt-05.ebs.msft:8079/resource-new-api/api";
    private static final String PROPERTY_VALUE_3 = "NovServPass";

    private static final String ENV = "DEV_NEW";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void validateIpAddressTest() throws UnauthorisedException {

        List<Client> clients = getClients();
        when(clientRepository.findAll()).thenReturn(clients);

        Property property = getProperty(ENV, PROPERTY_ITEM_1, PROPERTY_VALUE_1);
        when(propertyRepository.findByItemEnvironment(PROPERTY_ITEM_1, ENV)).thenReturn(property);

        String value = service.getValue(ENV, PROPERTY_ITEM_1, DORIAN_HOME_IP_ADDRESS);

        assertTrue(Objects.equals(value, PROPERTY_VALUE_1));
    }

    @Test
    public void validateIpAddressSubnetMaskTest() throws UnauthorisedException {

        List<Client> clients = getClients();
        when(clientRepository.findAll()).thenReturn(clients);

        exception.expect(UnauthorisedException.class);
        String value = service.getValue(ENV, PROPERTY_ITEM_1, "127.0.0.1");

        assertTrue(Objects.equals(value, PROPERTY_VALUE_1));
    }

    @Test
    public void validateIpAddressSubnetMaskTest_Success() throws UnauthorisedException {

        List<Client> clients = getClients();
        when(clientRepository.findAll()).thenReturn(clients);

        Property property = getProperty(ENV, PROPERTY_ITEM_1, PROPERTY_VALUE_1);
        when(propertyRepository.findByItemEnvironment(PROPERTY_ITEM_1, ENV)).thenReturn(property);

        String value = service.getValue(ENV, PROPERTY_ITEM_1, "10.42.1.120");

        assertTrue(Objects.equals(value, PROPERTY_VALUE_1));
    }

    @Test
    public void validateIpAddressSubnetMaskTest_Failure() throws UnauthorisedException {

        List<Client> clients = getClients();
        when(clientRepository.findAll()).thenReturn(clients);

        exception.expect(UnauthorisedException.class);
        String value = service.getValue(ENV, PROPERTY_ITEM_1, "10.43.1.120");

        assertTrue(Objects.equals(value, PROPERTY_VALUE_1));
    }

    private List<Property> getProperties() {
        Property property1 = getProperty(ENV, PROPERTY_ITEM_1, PROPERTY_VALUE_1);

        Property property2 = getProperty(ENV, PROPERTY_ITEM_2, PROPERTY_VALUE_2);

        Property property3 = getProperty(ENV, PROPERTY_ITEM_3, PROPERTY_VALUE_3);

        return Stream.of(property1, property2, property3).collect(toList());
    }

    private Property getProperty(String env, String item, String value) {
        Property property = new Property();
        PropertyId id = new PropertyId();
        id.setEnvironment(env); id.setItem(item);
        property.setId(id); property.setValue(value);
        return property;
    }

    private List<Client> getClients() {
        Client client1 = new Client();
        client1.setIpAddress(DORIAN_HOME_IP_ADDRESS); client1.setName("Dorian Home");
        Client client2 = new Client();
        client2.setIpAddress(DORIAN_WORK_IP_ADDRESS); client2.setName("Dorian Ntt");
        Client client3 = new Client();
        client3.setIpAddress(RANCHER_SUBNET_IP_ADDRESSES); client3.setName("Private IP address range: Rancher");

        return Stream.of(client1, client2, client3).collect(toList());
    }
}
