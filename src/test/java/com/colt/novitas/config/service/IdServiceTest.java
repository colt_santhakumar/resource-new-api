package com.colt.novitas.config.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.colt.novitas.config.dao.RequestIdRepository;
import com.colt.novitas.config.dao.ServiceIdRepository;
import com.colt.novitas.config.entity.RequestId;
import com.colt.novitas.config.entity.ServiceId;
import com.colt.novitas.config.infrastructure.pojos.ResourceChangeMessage;
import com.colt.novitas.config.request.IdType;
import com.colt.novitas.test.category.UnitTest;


@Category(UnitTest.class)
@RunWith(MockitoJUnitRunner.class)
public class IdServiceTest {
	
	@InjectMocks
	private IdServiceImpl id_service;
	
	@Mock
	private RequestIdRepository requestIdRepository;
	
	@Mock
	private ServiceIdRepository serviceIdRepository;
	
	@Test
	public void testUpdateRequestId() {
		
		ResourceChangeMessage rcm = new ResourceChangeMessage();
		RequestId requestid_B = new RequestId();
		
		rcm.setResourceId("1");
		rcm.setNovitasServiceId("novserid_demo_b");
		rcm.setStatus("unknown");
		
		Mockito.when(requestIdRepository.getRequestIdType(1))
    	.thenReturn(requestid_B);
				
		id_service.updateRequestId(rcm);
		String result = requestid_B.getNovitasServiceId();
		
		assertEquals(result , "novserid_demo_b");
	
	}
	

	@Test
	public void testUpdateServiceId() {
		
		ResourceChangeMessage rcmm = new ResourceChangeMessage();
		ServiceId serviceid_B = new ServiceId();	
		
		rcmm.setResourceId("2");
		rcmm.setNovitasServiceId("nsa");
		rcmm.setStatus("unknown");
	
		Mockito.when(serviceIdRepository.getServiceIdType("2"))
    	.thenReturn(serviceid_B);

		id_service.updateServiceId(rcmm);
		String result = serviceid_B.getNovitasServiceId();

		assertEquals(result , "nsa");
	
	}

	
	@Test
	public void testGetRequestIds() {
		
		RequestId a = new RequestId(); a.setType(IdType.PORT); a.setId(1);
		RequestId b = new RequestId(); b.setType(IdType.PORT); b.setId(null);
		RequestId c = new RequestId(); c.setType(null); c.setId(3);
		RequestId d = new RequestId(); d.setType(null); d.setId(null);
		
		List<RequestId> requests = new ArrayList<RequestId>();
		requests.add(a);
		requests.add(b);
		requests.add(c);
		requests.add(d);
		
		Mockito.when(requestIdRepository.getTopAllRequestsByRang(0, 4))
		.thenReturn(requests);
		
		Map<Integer,IdType> requestid_2 = id_service.getRequestIds(0, 4);
		
		assertEquals(4, requests.size());
		assertEquals(2, requestid_2.size());
		
	}	

}
