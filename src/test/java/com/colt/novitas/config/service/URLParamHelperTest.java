package com.colt.novitas.config.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.colt.novitas.config.util.URLParamHelper;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class URLParamHelperTest {
	
	@Test
	public void testDecodeParams() {	
		String name = "Test%23";
		String encodedName = URLParamHelper.decodeParams(name);				
		assertEquals("Test#", encodedName);
	}
}
