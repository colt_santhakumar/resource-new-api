package com.colt.novitas.config.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.colt.novitas.config.dao.ServiceSequenceDao;
import com.colt.novitas.config.entity.ServiceId;
import com.colt.novitas.config.request.Service;
import com.colt.novitas.config.util.ObjectConverter;
import com.colt.novitas.test.category.UnitTest;

@Category(UnitTest.class)
@RunWith(MockitoJUnitRunner.class)
public class ServiceSequenceTest {
	
	private static final String DECODED_NAME = "Test#";
	private static final String NOVITAS_SERVICE_ID = "001";
	
	@InjectMocks
	private ServiceSequenceImpl service;
	    
    @Mock
    private ServiceSequenceDao serviceSeqDao;
    
    @Mock
    private ObjectConverter converter;
    
    @Test
    public void testGetServicesByNameAndNovitasServiceId() {  
    	
    	List<ServiceId> servicesIds = getServiceIdList();
    	List<Service> services = getServicesList();   	
    	when(serviceSeqDao.findByNameAndNovitasServiceId(DECODED_NAME, NOVITAS_SERVICE_ID))
    	.thenReturn(servicesIds);
    	when(converter.convert(servicesIds, Service.class))
    	.thenReturn(services);
    	
    	List<Service> servList = service.getServicesByNameAndNovitasServiceId("Test%23", NOVITAS_SERVICE_ID);
        assertNotNull(servList);
        assertEquals(services, servList);
    }
    
    private List<ServiceId> getServiceIdList(){
    	List<ServiceId> listServices = new ArrayList<ServiceId>();
    	ServiceId serv = new ServiceId();
    	serv.setId(1);
    	serv.setName("Test#");
    	serv.setNovitasServiceId("001");
    	serv.setStatus("ACTIVE");
    	listServices.add(serv);
    	
    	return listServices;
    }
    
    private List<Service> getServicesList(){
    	List<Service> listServices = new ArrayList<Service>();
    	Service serv = new Service();
    	serv.setId(1);
    	serv.setName("Test#");
    	serv.setNovitasServiceId(001);
    	serv.setStatus("ACTIVE");
    	serv.setType("CLOUD_PORT");
    	listServices.add(serv);
    	
    	return listServices;
    }

}
