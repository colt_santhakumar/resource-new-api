
package com.colt.novitas.config.service;

import java.util.*;

import com.colt.novitas.config.controller.Response;
import com.colt.novitas.config.infrastructure.ConfigurationChangesNotifier;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.colt.novitas.config.dao.ClientRepository;
import com.colt.novitas.config.dao.PropertyRepository;
import com.colt.novitas.config.entity.Client;
import com.colt.novitas.config.entity.Property;
import com.colt.novitas.config.exception.UnauthorisedException;
import com.colt.novitas.test.category.UnitTest;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
@RunWith(MockitoJUnitRunner.class)
public class ConfigurationServiceTest {
	
	@InjectMocks
	private ConfigurationServiceImpl configService;
	    
    @Mock
    private PropertyRepository propertyRepository;
    
    @Mock
    private ClientRepository clientRepository;

    @Mock
    private ConfigurationChangesNotifier configurationChangesNotifier;

    private static final String DEV = "DEV";
    private static final String PROD = "PROD";
    private static final String IP = "127.0.0.1";
    
    final String key = "some.prop.key";
    final String value = "asdf7asdf8asdf";
    
    private Map<String, String> clients;
    private List<Property> properties;

    private static final String ITEM_1 = "httptimeoutminutes";
    private static final String VALUE_1 = "5";

    private static final String ITEM_2 = "mock.blueplanet";
    private static final String VALUE_2 = "true";

    private static final String ITEM_3 = "admin.api.login.url";
    private static final String VALUE_3 = "http://amsdcp13:8330/admin-api/api/login";

    private static final String ITEM_4 = "admin.api.customer.url";
    private static final String VALUE_4 = "http://amsdcp13:8550/admin-api/api/login";

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() {

        properties = new ArrayList<>();

        properties.add(Property.create(ITEM_1, VALUE_1, DEV));
        properties.add(Property.create(ITEM_2, VALUE_2, DEV));
        properties.add(Property.create(ITEM_3, VALUE_3, DEV));
        properties.add(Property.create(ITEM_4, VALUE_4, DEV));

        clients = new HashMap<>();

        clients.put("192.168.1.189", "Dev Server");
        clients.put("156.98.123.4", "Staging Server");
        clients.put(IP, "local test");
    }

    @Test
    public void getItemsByEnvironmentTest() throws Exception {
        when(clientRepository.findAll()).thenReturn(getClients());
        when(propertyRepository.findAll()).thenReturn(properties);

        configService.reload();
        List<Response> responseItems = configService.getItemsByEnvironment(DEV, IP);

        assertTrue(Objects.nonNull(responseItems));

        List<String> keys = Arrays.asList(ITEM_1, ITEM_2, ITEM_3, ITEM_4);
        List<String> values = Arrays.asList(VALUE_1, VALUE_2, VALUE_3, VALUE_4);

        for (Response response : responseItems) {
            assertTrue(response.getEnvironment().equals(DEV));
            assertTrue(keys.contains(response.getItem()));
            assertTrue(values.contains(response.getValue()));
        }
    }

    /**
     * Test when a value is found in the cache
     */
    @Test
    public void test_whenGetValue2param_thenReturnPropertyValue() {   
    	
    	when(propertyRepository.findByItemEnvironment("admin.api.login.url",DEV))
    	.thenReturn(Property.create("admin.api.login.url", "http://amsdcp13:8330/admin-api/api/login",DEV));
    	
    	//when
    	 String foundvalue = configService.getValue(DEV, "admin.api.login.url");
    	
    	//then
        assertNotNull(foundvalue);
        assertEquals("http://amsdcp13:8330/admin-api/api/login", foundvalue);
    }
    
    /**
     * Test when a value is null by passing wrong input
     * Unit test correct
     */
    @Test
    public void test_whenGetValue2param_thenReturnNullValue() {   
    	
    	//when given wrong value
    	 String null_value = configService.getValue(PROD, "admin.api.login.url");
    	
    	//then
        assertNull(null_value);

    }
    
    
    /** 
     * Test when a value is found in db, but not in cache
     * Unit test correct
     */
    @Test
    public void test_whenGetValue3param_thenReturnPropertyValue() throws UnauthorisedException {   
    		
    	when(propertyRepository.findByItemEnvironment(key , DEV))
    	.thenReturn(Property.create(key, value, DEV));
    	
    	when(clientRepository.findAll())
    	.thenReturn(getClients());
    	
    	//when
    	String foundvalue = configService.getValue(DEV, key, "156.98.123.4");
    	
    	//then
        assertNotNull(foundvalue);
        assertEquals(value, foundvalue);
    }
    
    
    /** 
     * Test when a value is not found and returns null
     * Unit test correct
     */
    @Test
    public void test_whenGetValue_thenReturnNullValue() throws UnauthorisedException {   
    	 	
    	when(propertyRepository.findByItemEnvironment(key , DEV))
    	.thenReturn(null);
    	
    	when(clientRepository.findAll())
    	.thenReturn(getClients());
    	
    	//when
    	String nullvalue = configService.getValue(DEV, key,"156.98.123.4");
    	
    	//then
        assertNull(nullvalue); 	
    }
    
    
    /** 
     * Test when an IP is Unauthorised
     * (expected = UnauthorisedException.class)
     */
    @Test  
    public void test_whenGetValue_thenReturnUnauthorised() throws UnauthorisedException {   
    	assertThrows(UnauthorisedException.class, () -> configService.getValue(DEV, "admin.api.login.url", "156.99.123.4"));
    }

    
    /**
    * Test reload successfully
    */

    @Test
    public void testReloadSuccess() {
        when(clientRepository.findAll()).thenReturn(getClients());

        configService.reload();
    }
	    
    private List<Client> getClients() {

        List<Client> whitelist = new ArrayList<Client>();
        
        for (String key : clients.keySet()) {
            Client client = new Client();
            client.setIpAddress(key);
            client.setName(clients.get(key));
            whitelist.add(client);
        }
        return whitelist;
    }
}
