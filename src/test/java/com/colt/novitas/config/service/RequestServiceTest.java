package com.colt.novitas.config.service;

import com.colt.novitas.config.dao.RequestSequenceDao;
import com.colt.novitas.config.entity.RequestId;
import com.colt.novitas.config.request.Request;
import com.colt.novitas.test.category.UnitTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
@RunWith(MockitoJUnitRunner.class)
public class RequestServiceTest {
	
	private static final String DECODED_NAME = "Test#";
	private static final String NOVITAS_SERVICE_ID = "001";
	private static final String NAME= "Test%23";
	
	@InjectMocks
	private RequestServiceImpl service;
	    
    @Mock
    private RequestSequenceDao requestSeqDao;
    
    @Test
    public void testGetRequestByNameAndNovitasServiceId() {
    	List<RequestId> requestIds = getRequestIdList();
    	when(requestSeqDao.findAll(any(Specification.class))).thenReturn(requestIds);
    	List<Request> reqList = service.getRequestsBySearchCriteria(NAME, NOVITAS_SERVICE_ID, null, null);
        assertNotNull(reqList);
    }
    
    private List<RequestId> getRequestIdList(){
    	List<RequestId> listRequests = new ArrayList<RequestId>();
    	RequestId req = new RequestId();
    	req.setId(1);
    	req.setName(DECODED_NAME);
    	req.setNovitasServiceId("001");
    	req.setStatus("ACTIVE");
    	listRequests.add(req);
    	
    	return listRequests;
    }
}
