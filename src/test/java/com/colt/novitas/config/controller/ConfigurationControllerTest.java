package com.colt.novitas.config.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.Mockito.doThrow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.colt.novitas.config.exception.UnauthorisedException;
import com.colt.novitas.config.service.ConfigurationService;
import com.colt.novitas.test.category.UnitTest;
import com.fasterxml.jackson.databind.ObjectMapper;


@Category(UnitTest.class)
@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class ConfigurationControllerTest {

    private static final String ENV = "DEV";
    private static final String ITEM = "admin.api.login.url";
    private static final String VALUE = "http://amsdcp13:8330/admin-api/api/customer";
    private static final String IP = "127.0.0.1";

    private static final String ITEM_1 = "request.deletecloudconnectionurl";
    private static final String VALUE_1 = "/cloudrequests/connection/{connection_id}";

    private static final String ITEM_2 = "request.deletecloudporturl";
    private static final String VALUE_2 = "/cloudrequests/port/{port_id}";

    private static final String ITEM_3 = "request.deleteconnectionurl";
    private static final String VALUE_3 = "/requests/connection/{connection_id}";

    private static final String ITEM_4 = "request.deleteporturl";
    private static final String VALUE_4 = "/requests/port/{port_id}";

    @InjectMocks
    private ConfigurationController controller;

    @Mock
    private ConfigurationService service;

    @Mock
    HttpServletRequest request;

    private MockMvc mockMvc;
    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setup() {

        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .defaultRequest(get("/")
                        .accept(MediaType.APPLICATION_JSON))
                .build();
    }

    @Test
    public void getItemsForEnvironmentTest() throws Exception {

        List<Response> items = getItems();
        when(service.getItemsByEnvironment(ENV, IP)).thenReturn(items);

        MvcResult result = mockMvc
                .perform(get("/configuration").param("environment", ENV))
                .andExpect(status().isOk())
                .andReturn();

        String json = result.getResponse().getContentAsString();
        assertNotNull(json);

        List<Response> responseItems = mapper.readValue(json, new TypeReference<List<Response>>() {});
        assertNotNull(responseItems);

        List<String> keys = Arrays.asList(ITEM_1, ITEM_2, ITEM_3, ITEM_4);
        List<String> values = Arrays.asList(VALUE_1, VALUE_2, VALUE_3, VALUE_4);

        for (Response response : responseItems) {
            assertTrue(response.getEnvironment().equals(ENV));
            assertTrue(keys.contains(response.getItem()));
            assertTrue(values.contains(response.getValue()));
        }
    }

    private List<Response> getItems() {
        Response response1 = new Response(ENV, ITEM_1, VALUE_1);
        Response response2 = new Response(ENV, ITEM_2, VALUE_2);
        Response response3 = new Response(ENV, ITEM_3, VALUE_3);
        Response response4 = new Response(ENV, ITEM_4, VALUE_4);
        return Arrays.asList(response1, response2, response3, response4);
    }

    /**
     * Test getItem when Item list size equals 1
     * Check response is built with parameters given
     * Check that responseEntity is return when calling method and HttpStatus is OK
     *
     * @throws Exception
     */
    @Test
    public void testGetItemWhenSize1() throws Exception {

        //given
        List<String> items = new ArrayList<String>();
        items.add(ITEM);

        //mocking
        when(service.getValue(ENV, ITEM, IP)).thenReturn(VALUE);
        MvcResult result = mockMvc
                .perform(get("/configuration").param("environment", ENV).param("item", ITEM))
                .andExpect(status().isOk()).andReturn();

        //build response
        String json = result.getResponse().getContentAsString();
        assertNotNull(json);
        Response response = mapper.readValue(json, Response.class);

        //validation of response
        assertNotNull(response);
        assertEquals(ENV, response.getEnvironment());
        assertEquals(ITEM, response.getItem());
        assertEquals(VALUE, response.getValue());

        //call method under test
        ResponseEntity<?> responseEntity = controller.getItem(ENV, items, request);
        //validate return responseEntity: list size and httpstatus.ok
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity);
        assertEquals("admin.api.login.url", items.get(0));
        assertEquals(1, items.size());

    }


    /**
     * Test getItem when Item list size more than 1
     * Check responseEntity is return with Httpstatus OK
     * Check that a list of response is return with size 2
     *
     * @throws Exception
     */
    @Test
    public void testGetListOfItem() throws Exception {

        List<String> items = new ArrayList<String>();
        items.add("admin.api.login.url");
        items.add("request.api.login.url");

        when(service.getValue(ENV, ITEM, IP)).thenReturn(VALUE);
        when(request.getRemoteAddr()).thenReturn(IP);

        ResponseEntity<?> responseEntity = controller.getItem(ENV, items, request);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        List<Response> list_response = (List<Response>) responseEntity.getBody();
        assertEquals(2, list_response.size());

    }

    /**
     * Test when unknown values is passed, and return empty response
     *
     * @throws Exception
     */
    @Test
    public void testGetItemUnknown() throws Exception {

        //given
        List<String> items = new ArrayList<String>();
        items.add("");

        //when
        when(service.getValue(ENV, "", IP)).thenReturn(VALUE);
        when(request.getRemoteAddr()).thenReturn(IP);

        //then calling method under test
        ResponseEntity<?> responseEntity = controller.getItem(ENV, items, request);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Response response = (Response) responseEntity.getBody();

        //validate response item is empty
        assertTrue(response.getItem().isEmpty());

    }


    /**
     * The class under test throws UnauthorisedException via getValue.
     *
     * @throws Exception (expected = UnauthorisedException.class)
     */
    @Test
    public void testGetItemUnauthorised() throws Exception {

        //given
        List<String> items = new ArrayList<String>();
        items.add("admin.api.login.url");
        items.add("request.api.login.url");

        //mocking methods
        when(service.getValue(ENV, ITEM, IP)).thenThrow(new UnauthorisedException());
        when(request.getRemoteAddr()).thenReturn(IP);

        //when calling method under test returns exception
        assertThrows(UnauthorisedException.class, () -> controller.getItem(ENV, items, request));

        //ResponseEntity<?> responseEntity  = controller.getItem( ENV, items, request);

    }

    @Test
    public void testGetItemEnvironmentNull() throws Exception {

        //given
        List<String> items = new ArrayList<String>();
        items.add("admin.api.login.url");
        items.add("request.api.login.url");

        //then calling method under test
        ResponseEntity<?> responseEntity = controller.getItem(null, items, request);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

    }


    /**
     * test when passing request and return responseEntity
     *
     * @throws Exception
     */
    @Test
    public void testReload() throws Exception {

        //then call method under test
        ResponseEntity<?> responseEntity = controller.reload();

        //validate return values
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

}
