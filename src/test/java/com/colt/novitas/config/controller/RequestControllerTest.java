package com.colt.novitas.config.controller;

import com.colt.novitas.config.request.Request;
import com.colt.novitas.config.service.RequestServiceImpl;
import com.colt.novitas.test.category.UnitTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@Category(UnitTest.class)
@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class RequestControllerTest {
	
	private static final String ENCODED_NAME = "Test%23";
	private static final String NOVITAS_SERVICE_ID = "001";
	
	@InjectMocks
    private RequestController controller;

    @Mock
    private RequestServiceImpl service;
    
    @Test
    public void getRequestsByEncodedNameTest() throws Exception {
    	
    	 List<Request> listRequests = getRequestsList();
		 Mockito.when(service.getRequestsBySearchCriteria(ENCODED_NAME, NOVITAS_SERVICE_ID, null, null)).thenReturn(listRequests);

		 ResponseEntity<?> responseEntity = controller.getRequests(ENCODED_NAME, NOVITAS_SERVICE_ID, null, null);
		 assertEquals(HttpStatus.OK , responseEntity.getStatusCode());
		 assertEquals(listRequests , responseEntity.getBody());   	
    }
       
    private List<Request> getRequestsList(){
    	List<Request> listRequests = new ArrayList<Request>();
    	Request req = new Request();
    	req.setId(1);
    	req.setName("Test#");
    	req.setNovitasServiceId("001");
    	req.setStatus("ACTIVE");
    	req.setType("CLOUD_PORT");
    	listRequests.add(req);
    	
    	return listRequests;
    }
}
