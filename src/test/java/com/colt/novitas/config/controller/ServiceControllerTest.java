package com.colt.novitas.config.controller;

import com.colt.novitas.config.request.CreateServiceRequest;
import com.colt.novitas.config.request.Service;
import com.colt.novitas.config.request.ServiceStatus;
import com.colt.novitas.config.request.ServiceType;
import com.colt.novitas.config.service.ServiceSequenceImpl;
import com.colt.novitas.test.category.UnitTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Category(UnitTest.class)
@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class ServiceControllerTest {
    private static final String ENCODED_NAME = "Test%23";
    private static final String NOVITAS_SERVICE_ID = "001";

    @InjectMocks
    private ServiceController controller;

    @Mock
    private ServiceSequenceImpl service;


    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void getServicessByEncodedNameTest() throws Exception {

        List<Service> listServices = getServicesList();
        Mockito.when(service.getServicesByNameAndNovitasServiceId(ENCODED_NAME, NOVITAS_SERVICE_ID)).thenReturn(listServices);

        ResponseEntity<?> responseEntity = controller.getServices(ENCODED_NAME, NOVITAS_SERVICE_ID);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(listServices, responseEntity.getBody());
    }

    @Test
    public void createServicesRequest_with_nulls_Test() throws Exception {
        CreateServiceRequest createService = new CreateServiceRequest();
        createService.setId("8522223223");
        createService.setName("TEST");
        List<ConstraintViolation<CreateServiceRequest>> violations = validator.validate(createService)
                .stream()
                .sorted(Comparator.comparingInt(a -> a.getMessage().length())).collect(Collectors.toList());
        assertEquals(3, violations.size());
        // check all missing field and there constrain
        assertEquals("request field not match with PORT", violations.get(0).getMessage());
        assertEquals("request field not match with ACTIVE", violations.get(1).getMessage());
        assertEquals("parent_service_id is mandatory field", violations.get(2).getMessage());
    }

    @Test
    public void createServicesRequestTest() throws Exception {
        CreateServiceRequest createService = new CreateServiceRequest();
        createService.setId("8522223223");
        createService.setName("TEST");
        createService.setParent_service_id("2356");
        createService.setType(ServiceType.PORT);
        createService.setStatus(ServiceStatus.ACTIVE);
        Set<ConstraintViolation<CreateServiceRequest>> violations = validator.validate(createService);
        assertTrue(violations.isEmpty());
        Mockito.doNothing().when(service).createServiceManual(createService);
        controller.createService(createService);
        Mockito.verify(service, Mockito.times(1)).createServiceManual(createService);
    }

    private List<Service> getServicesList() {
        List<Service> listServices = new ArrayList<Service>();
        Service serv = new Service();
        serv.setId(8001);
        serv.setName("Test#");
        serv.setNovitasServiceId(001);
        serv.setStatus("ACTIVE");
        serv.setType("CLOUD_PORT");
        listServices.add(serv);

        return listServices;
    }


}
