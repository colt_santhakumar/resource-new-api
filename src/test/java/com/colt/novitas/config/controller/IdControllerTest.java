package com.colt.novitas.config.controller;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.exceptions.base.MockitoException;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.test.context.web.WebAppConfiguration;

import com.colt.novitas.config.controller.IdController;
import com.colt.novitas.config.request.IdType;
import com.colt.novitas.config.service.IdServiceImpl;
import com.colt.novitas.test.category.UnitTest;


@Category(UnitTest.class)
@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class IdControllerTest {
	
	@InjectMocks
	private IdController controller;

	@Mock
	private IdServiceImpl service;
	
	@Test
	public void testcreateRequestId_whenHttpStatusCreated() throws Exception{
		
		//given
		Integer id = 1;
	    Optional<Integer> opt = Optional.ofNullable(id);
		//when
		Mockito.when(service.createRequestId(IdType.PORT)).thenReturn(opt);
		//then
		ResponseEntity<Integer> responseEntity = controller.createRequestId(IdType.PORT);
		//validate
		assertEquals(HttpStatus.CREATED , responseEntity.getStatusCode());
		assertEquals( id, responseEntity.getBody());	
		
	}
	
	@Test
	public void testcreateRequestId_whenHttpStatusBadRequest() throws Exception{
		
		//given
		Integer id = null;
	    Optional<Integer> opt = Optional.ofNullable(id);
		//when
		Mockito.when(service.createRequestId(IdType.PORT)).thenReturn(opt);
		//then
		ResponseEntity<Integer> responseEntity = controller.createRequestId(IdType.PORT);
		//validate
		assertEquals(HttpStatus.BAD_REQUEST , responseEntity.getStatusCode());
		assertEquals( id, responseEntity.getBody());	
		
	}
	
	@Test
	public void testcreateRequestId_whenNull() throws Exception{
		
		//given
		Integer id = null;
	    Optional<Integer> opt = Optional.ofNullable(id);
		//when
		Mockito.when(service.createRequestId(null)).thenReturn(opt);
		//then
		ResponseEntity<Integer> responseEntity = controller.createRequestId(null);
		//validate
		assertEquals(HttpStatus.BAD_REQUEST , responseEntity.getStatusCode());
		
	}
	
	@Test
	public void testcreateServiceId_whenHttpStatusCreated() throws Exception{
		
		//given
		String type = "type";
	    Optional<String> opt = Optional.ofNullable(type);
		//when
		Mockito.when(service.createServiceId(IdType.PORT)).thenReturn(opt);
		//then
		ResponseEntity<String> responseEntity = controller.createServiceId(IdType.PORT);
		//validate
		assertEquals(HttpStatus.CREATED , responseEntity.getStatusCode());
		assertEquals( type, responseEntity.getBody());	
		
	}
	
	@Test
	public void testcreateServiceId_whenHttpStatusBadRequest() throws Exception{
		
		//given
		String type = null;
	    Optional<String> opt = Optional.ofNullable(type);
		//when
		Mockito.when(service.createServiceId(IdType.PORT)).thenReturn(opt);
		//then
		ResponseEntity<String> responseEntity = controller.createServiceId(IdType.PORT);
		//validate
		assertEquals(HttpStatus.BAD_REQUEST , responseEntity.getStatusCode());
		assertEquals( type , responseEntity.getBody());	
		
	}
	
	@Test
	public void testcreateServiceId_whenNull() throws Exception{
		
		//given
		String type = null;
	    Optional<String> opt = Optional.ofNullable(type);
		//when
		Mockito.when(service.createServiceId(null)).thenReturn(opt);
		//then
		ResponseEntity<String> responseEntity = controller.createServiceId(null);
		//validate
		assertEquals(HttpStatus.BAD_REQUEST , responseEntity.getStatusCode());
		assertEquals( type , responseEntity.getBody());	
		
	}
	
	
	@Test
	public void testgetRequestIdType_whenHttpStatusOK() throws Exception{
		
		 //given
	     Optional<IdType> opt = Optional.ofNullable(IdType.PORT);
		 //when
		 Mockito.when(service.getRequestIdType(1)).thenReturn(opt);
		 //then
		 ResponseEntity<String> responseEntity = controller.getRequestIdType(1);
		 //validate
		 assertEquals(HttpStatus.OK , responseEntity.getStatusCode());
		 assertEquals("PORT" , responseEntity.getBody());	
	}
	
	@Test
	public void testgetRequestIdType_whenHttpStatusNotFound() throws Exception{
		
		 //given
	     Optional<IdType> opt = Optional.ofNullable(null);
		 //when
		 Mockito.when(service.getRequestIdType(1)).thenReturn(opt);
		 //then
		 ResponseEntity<String> responseEntity = controller.getRequestIdType(1);
		 //validate
		 assertEquals(HttpStatus.NOT_FOUND , responseEntity.getStatusCode());
		 assertEquals(null , responseEntity.getBody());	
	}
	
	@Test
	public void testgetRequestIdType_whenNull() throws Exception{
		
		 //then
		 ResponseEntity<String> responseEntity = controller.getRequestIdType(null);
		 //validate
		 assertEquals(HttpStatus.NOT_FOUND , responseEntity.getStatusCode());
		 assertEquals(null , responseEntity.getBody());	
	}
	
	@Test
	public void testgetServiceIdType_whenHttpStatusOK() throws Exception{
		
		 //given
	     Optional<IdType> opt = Optional.ofNullable(IdType.PORT);
		 //when
		 Mockito.when(service.getServiceIdType("PORT")).thenReturn(opt);
		 //then
		 ResponseEntity<String> responseEntity = controller.getServiceIdType("PORT");
		 //validate
		 assertEquals(HttpStatus.OK , responseEntity.getStatusCode());
		 assertEquals("PORT" , responseEntity.getBody());	
	}
	
	@Test
	public void testgetServiceIdType_whenHttpStatusNotFound() throws Exception{
		
		 //given
	     Optional<IdType> opt = Optional.ofNullable(null);
		 //when
		 Mockito.when(service.getServiceIdType("PORT")).thenReturn(opt);
		 //then
		 ResponseEntity<String> responseEntity = controller.getServiceIdType("PORT");
		 //validate
		 assertEquals(HttpStatus.NOT_FOUND , responseEntity.getStatusCode());
		 assertEquals(null , responseEntity.getBody());	
	}
	
	@Test
	public void testgetServiceIdType_whenNull() throws Exception{
		
		 //then
		 ResponseEntity<String> responseEntity = controller.getServiceIdType(null);
		 //validate
		 assertEquals(HttpStatus.NOT_FOUND , responseEntity.getStatusCode());
		 assertEquals(null , responseEntity.getBody());	
	}
	
	@Test
	public void testgetRequestbyId_whenHttpStatusOK() throws Exception{
		
		 //given
		 Map<Integer,IdType> opt = new HashMap<Integer,IdType>();
		 //when
		 Mockito.when(service.getRequestIds( Mockito.anyInt(), Mockito.anyInt())).thenReturn(opt);
		 //then
		 ResponseEntity<?> responseEntity = controller.getRequestById(Mockito.anyInt() , Mockito.anyInt());
		 //validate
		 assertEquals(HttpStatus.OK , responseEntity.getStatusCode());
		 assertEquals(opt , responseEntity.getBody());	
	}
	
	@Test
	public void testgetRequestbyId_whenNullHttpStatusOK() throws Exception{
		
		 //given
		 Map<Integer,IdType> opt = new HashMap<Integer,IdType>();
		 //when
		 Mockito.when(service.getRequestIds(Mockito.anyInt() ,Mockito.anyInt() )).thenReturn(opt);
		 //then
		 ResponseEntity<?> responseEntity = controller.getRequestById(null , null);
		 //validate
		 assertEquals(HttpStatus.OK , responseEntity.getStatusCode());
		 assertEquals(opt , responseEntity.getBody());	
	}
	
	@Test
	public void testgetRequestbyId_whenHttpStatusBadRequest() throws Exception{
		
		 //when
		 Mockito.when(service.getRequestIds(Mockito.anyInt() , Mockito.anyInt())).thenThrow(new MockitoException(null));
		 //then
		 ResponseEntity<?> responseEntity = controller.getRequestById(Mockito.anyInt() , Mockito.anyInt());
		 //validate
		 assertEquals(HttpStatus.BAD_REQUEST , responseEntity.getStatusCode());
			
	}

}


