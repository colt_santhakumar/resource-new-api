package com.colt.novitas;

import com.colt.novitas.config.entity.Client;
import org.springframework.security.web.util.matcher.IpAddressMatcher;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class WhitelistIpsThreadTest {

//    private static List<String> whitelistIps = new ArrayList<>(); //replacing CopyOnWriteArrayList with simple ArrayList will throw ConcurrentModificationException
    private static CopyOnWriteArrayList<String> whitelistIps = new CopyOnWriteArrayList<>();

    private static Deque<Integer> POSITIONS = new LinkedList<>();

    public static void main(String... args) {

        for (int i = 0; i < 200; i++) {
            POSITIONS.add(i);
        }

        String ipAddress = "10.224.0.";

        for (int i = 0; i < 199; i++) {
            Thread thread = new Thread(() -> {
                try {
                    validateAccess(ipAddress + POSITIONS.removeFirst());
                } catch (Exception e) {
                    System.out.println("Something happened: " + e.getMessage());
                    e.printStackTrace();
                }
            });
            thread.setName("Worker " + i);
            System.out.println("Starting worker " + i);
            thread.start();
        }
    }

    private static void validateAccess(String ipAddress) {

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (!whitelistIps.contains(ipAddress) && !isIpPartOfASubnetMask(ipAddress)) {

            List<Client> clients = getClients();

            for (Client client : clients) {
                whitelistIps.add(client.getIpAddress());
            }

            if (!whitelistIps.contains(ipAddress) && !isIpPartOfASubnetMask(ipAddress)) {
                System.out.println("Worker: " + Thread.currentThread().getName() + " does not found the ipaddress: " + ipAddress);
            }
        }
    }

    private static List<Client> getClients() {
        List<Client> clients = new ArrayList<>();

        Client client1 = new Client();
        client1.setName("client1");
        client1.setName("10.224.13.161");

        Client client2 = new Client();
        client2.setName("client2");
        client2.setName("10.224.13.162");

        Client client3 = new Client();
        client3.setName("client3");
        client3.setName("10.224.13.163");

//        Client client4 = new Client();
//        client4.setName("client4");
//        client4.setName("10.224.0.0/16");

//        clients.add(client1); clients.add(client2); clients.add(client3); clients.add(client4);
        clients.add(client1); clients.add(client2); clients.add(client3);

        return clients;
    }

    private static boolean isIpPartOfASubnetMask(String ipAddress) {
        return Objects.nonNull(getSubnetMaskMatchingIp(ipAddress));
    }

    private static String getSubnetMaskMatchingIp(String ipAddress) {
        return whitelistIps
                .stream()
                .filter(ip -> isPrivateManagedIps(ipAddress))
                .filter(whitelistIp -> ipMatchesSubnetMask(whitelistIp, ipAddress))
                .findFirst()
                .orElse(null);
    }

    private static boolean isPrivateManagedIps(String ipAddress) {
        String[] addressAndMask = StringUtils.split(ipAddress, "/");

        if (Objects.nonNull(addressAndMask) && addressAndMask.length > 1) {
            try {
                Integer.parseInt(addressAndMask[1]);
                return true;
            } catch (NumberFormatException nfe) {
                return false;
            }
        }

        return false;
    }

    private static boolean ipMatchesSubnetMask(String subnetMask, String ipAddress) {

        boolean matches = false;

        try {
            matches = new IpAddressMatcher(subnetMask).matches(ipAddress);
        } catch (IllegalArgumentException e) {
            //swallow
        }

        return matches;
    }
}
