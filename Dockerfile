FROM openjdk:8-jdk-alpine
EXPOSE 8079
VOLUME /tmp
WORKDIR /tmp
ARG JAR_FILE=/target/resource-new-api.jar
ADD ${JAR_FILE} resource-new-api.jar
ENTRYPOINT ["java", "-jar", "resource-new-api.jar", "--spring.config.location=file:/config/config-resource-new-api.properties"]